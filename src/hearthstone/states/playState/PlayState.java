package hearthstone.states.playState;

import Entity.cards.Deck;
import hearthstone.Handler;
import hearthstone.constants.Constants;
import hearthstone.gfx.Asserts;
import hearthstone.states.State;
import java.awt.*;
import java.util.Collections;

public class PlayState extends State {
    private ConstantButton constantButton;
    private GamePlayer gamePlayer;
    private Deck deckPlay;
    public PlayState(Handler handler) {
        super(handler);
        deckPlay = handler.getCollectionState().getPlayDeck();
        Collections.shuffle(deckPlay.getCards());
        gamePlayer = new GamePlayer(handler, deckPlay);
        constantButton = new ConstantButton(handler);
    }

    @Override
    public void tick() {
        constantButton.tick();
        gamePlayer.tick();
    }

    @Override
    public void render(Graphics2D g2D) {
        g2D.drawImage(Asserts.playBackground, 0, 0, Constants.COMPUTER_WIDTH, Constants.COMPUTER_HEIGHT, null);
        constantButton.render(g2D);
        gamePlayer.render(g2D);
    }

    public GamePlayer getGamePlayer() {
        return gamePlayer;
    }
}
