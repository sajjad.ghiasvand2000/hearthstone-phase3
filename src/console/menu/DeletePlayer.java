//package console.menu;
//
//import Entity.MainPlayer;
//import console.State;
//import data.Log;
//import data.MyFile;
//
//import java.io.IOException;
//import java.util.Scanner;
//
//public class DeletePlayer {
//
//    public static State setState() throws IOException {
//        Scanner scanner = new Scanner(System.in);
//        System.out.println("password:");
//        String pass = scanner.nextLine();
//        System.out.println("Are you sure if you want to delete your account?(y/n)");
//        String answer = scanner.nextLine();
//        if (answer.equals("n")) {
//            Log.body(MainPlayer.getInstance().getUserId(), MainPlayer.getInstance().getUsername(), "back", "return to main menu");
//            return State.enter;
//        }
//        else if (answer.equals("y")) {
//            MyFile.deletePlayer("player.txt", pass);
//            Log.deletePlayer(MainPlayer.getInstance().getUserId(), MainPlayer.getInstance().getUsername());
//            return State.preLogin;
//        }
//        else {
//            System.out.println("Your inputs are incorrect.Please try again!");
//            return State.deletePlayer;
//        }
//    }
//}
