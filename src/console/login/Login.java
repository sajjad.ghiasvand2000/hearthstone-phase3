//package console.login;
//
//import Entity.cards.Card;
//import Entity.Hero;
//import Entity.MainPlayer;
//import console.State;
//import data.Log;
//import data.MyFile;
//import data.Setter;
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Scanner;
//
//public class Login{
//
//    public static State setState() throws IOException {
//        Scanner scanner = new Scanner(System.in);
//        System.out.println("Userna");
//        String username = scanner.nextLine();
//        if (MyFile.isRepetitiveUsername(username, "player.txt")) {
//            System.out.println("This username has already existed please choose another one!");
//            return State.login;
//        }
//        System.out.println("Password:");
//        String password = scanner.nextLine();
//        int numberId = Log.userId();
//        List<Hero> heroes = Setter.setHero();
//        ArrayList<Card> cards = Setter.setCard();
//        MainPlayer.getInstance().setUserId(numberId);
//        MainPlayer.getInstance().setUsername(username);
//        MainPlayer.getInstance().setPassword(password);
//        MainPlayer.getInstance().setHeroes(heroes);
//        MainPlayer.getInstance().setEntireCards(cards);
//        MainPlayer.getInstance().setDecks(null);
//        MainPlayer.getInstance().setDiamond(50);
//        MyFile.fileWriter("player.txt", MainPlayer.getInstance());
//        Log.createLog(numberId, username, password);
//        System.out.println("Congratulation!\nYou created an account successfully.");
//        return State.enter;
//    }
//}
//
