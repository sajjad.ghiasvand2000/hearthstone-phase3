package Entity.cards;

import Entity.HeroClass;

import java.awt.image.BufferedImage;

public class Spell extends Card {
    private String QuestAndReward;


    public Spell(String name, int cost, int mana, Rarity rarity, HeroClass heroClass, Type type, String description, String questAndReward, String[] texturePath, boolean lock) {
        super(name, cost, mana, rarity, heroClass, type, description, texturePath, lock);
        QuestAndReward = questAndReward;
    }
}
