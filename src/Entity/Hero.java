package Entity;

import Entity.cards.Card;
import console.AddRemove;

import java.io.FileNotFoundException;
import java.util.List;

public class Hero extends AddRemove {

    private HeroClass heroClass;
    private int HP;
    private List<Card> cards;
    private String HeroPower;
    private String SpecialPower;
    private boolean isLock = false;
    private String[] texturePath;

    public Hero(HeroClass heroClass, int HP, String heroPower, String specialPower) {
        this.heroClass = heroClass;
        this.HP = HP;
        HeroPower = heroPower;
        SpecialPower = specialPower;
    }

    public Hero() {
    }

    public Hero(HeroClass heroClass) {
        this.heroClass = heroClass;
    }

    public List<Card> getCards() {
        return cards;
    }

    public void setCards(List<Card> cardsName) {
        this.cards = cardsName;
    }

    public HeroClass getHeroClass() {
        return heroClass;
    }

    public void setHeroClass(HeroClass heroClass) {
        this.heroClass = heroClass;
    }

    public void setLock(boolean lock) {
        isLock = lock;
    }

    public boolean getIsLock() {
        return isLock;
    }

    public boolean addRemoveCardName(String cardName, String addOrRemove) throws FileNotFoundException {
        return addRemoveCard(cardName, cards, addOrRemove);
    }

    public String[] getTexturePath() {
        return texturePath;
    }

    public void setTexturePath(String[] texturePath) {
        this.texturePath = texturePath;
    }
}
