package hearthstone.states;

import hearthstone.Handler;
import hearthstone.ui.UIManager;

import java.awt.*;
import java.util.ArrayList;

public abstract class State {
    private static State currentState = null;
    protected Handler handler;

    public State(Handler handler) {
        this.handler = handler;
    }

    public abstract void tick();

    public abstract void render(Graphics2D g2D);

    public static State getCurrentState() {
        return currentState;
    }

    public static void setCurrentState(State currentState) {
        State.currentState = currentState;
    }

}
