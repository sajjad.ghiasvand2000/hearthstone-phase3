package hearthstone.ui;

import hearthstone.Game;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.Objects;

public abstract class UIObject {
    protected float x, y;
    protected int width, height;
    protected boolean hovering = false;
    protected Rectangle bounds;
    protected BufferedImage[] images;
    protected ClickListener clicker;
    private int controllerCounter = 0;
    private boolean controller = false;

    public UIObject(float x, float y, int width, int height, BufferedImage[] bufferedImages, ClickListener clicker) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.images = bufferedImages;
        this.clicker = clicker;
        bounds = new Rectangle((int)x, (int)y, width, height);
    }

    public void tick(){
        if (controller){
            controllerCounter++;
            if (controllerCounter == 20){
                controllerCounter = 0;
                controller = false;
            }
        }
    }

    public abstract void render(Graphics2D g2D);

    public abstract void onClick();

    public void onMouseMove(){
        if (bounds.contains(Game.getMouseManager().getMouseX(), Game.getMouseManager().getMouseY()))
            hovering = true;
        else hovering = false;
    }

    public void onMouseRelease(){
        if (hovering && Game.getMouseManager().isLeftPressed())
            if (controllerCounter == 0) {
                onClick();
                controllerCounter++;
                controller = true;
            }
    }

    public void setX(float x) {
        this.x = x;
        bounds = new Rectangle((int)x, (int)y, width, height);
    }

    public void setY(float y) {
        this.y = y;
        bounds = new Rectangle((int)x, (int)y, width, height);
    }

    public void setImages(BufferedImage[] images) {
        this.images = images;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UIObject uiObject = (UIObject) o;
        return Float.compare(uiObject.x, x) == 0 &&
                Float.compare(uiObject.y, y) == 0 &&
                width == uiObject.width &&
                height == uiObject.height &&
                hovering == uiObject.hovering &&
                Objects.equals(bounds, uiObject.bounds) &&
                Arrays.equals(images, uiObject.images) &&
                Objects.equals(clicker, uiObject.clicker);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(x, y, width, height, hovering, bounds, clicker);
        result = 31 * result + Arrays.hashCode(images);
        return result;
    }
}
