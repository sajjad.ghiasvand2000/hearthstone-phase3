package hearthstone.configs;

public class ShopStateConfigs {
    private String name;
    private Configs properties;
    private static ShopStateConfigs shopStateConfigs;

    private ShopStateConfigs(String name){
        properties = ConfigLoader.getInstance().getShopStateConfigs(name);
    }

    public static ShopStateConfigs getInstance(String name){
        if (shopStateConfigs == null)
            shopStateConfigs = new ShopStateConfigs(name);
        return shopStateConfigs;
    }

    public static ShopStateConfigs getInstance(){
        return getInstance("SHOP_STATE_CONFIG_FILE");
    }

    public int get(String name){
        return properties.readInteger(name);
    }
}
