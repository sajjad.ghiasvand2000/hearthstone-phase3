//package console.collections;
//
//import Entity.cards.Card;
//import Entity.MainPlayer;
//import console.State;
//import data.Log;
//
//import java.io.IOException;
//import java.util.Scanner;
//
//public class Collections {
//
//    public static State setState() throws IOException {
//        Scanner scanner = new Scanner(System.in);
//        MainPlayer m = MainPlayer.getInstance();
//        System.out.println("All of your cards are:");
//        for (Card card : m.getEntireCards()) {
//            System.out.println(card.getName());
//        }
//        System.out.println("--------------------------------");
//        System.out.println("Choose one of these:\nls-a-heroes\nls-m-heroes\nhearthstone--help\nback");
//        String next1 = scanner.nextLine();
////        switch (next1) {
//            case "ls-a-heroes":
//                Log.body(m.getUserId(), m.getUsername(), "list", "heroes:all");
//                return State.ls_a_heroes;
//            case "ls-m-heroes":
//                return State.ls_m_heroes;
//            case "back":
//                Log.body(m.getUserId(), m.getUsername(), "back", "return to main menu");
//                return State.enter;
//            case "hearthstone--help":
//                System.out.println("ls-a-heroes:For showing all heroes.");
//                System.out.println("ls-m-heroes:For showing the hero which you will choose.");
//                System.out.println("hearthstone--help\nback");
//                System.out.println("----------------------------");
//                Log.body(m.getUserId(), m.getUsername(), "help", "hearthstone--help");
//                return State.collections;
//            default:
//                System.out.println("Inputs are incorrect please try again.");
//                Log.body(m.getUserId(), m.getUsername(), "error", "wrong inputs in collection");
//                return State.collections;
//        }
//    }
//}
