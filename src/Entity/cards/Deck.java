package Entity.cards;

import Entity.Hero;
import hearthstone.states.statusState.logic.StatusLogic;

import java.util.ArrayList;
import java.util.List;

public class Deck {
    private List<Card> cards;
    private Hero hero;
    private String texturePath;
    private String name;
    private int numberGame;
    private int numberWin;


    public Deck(List<Card> cards, Hero hero) {
        this.cards = cards;
        this.hero = hero;
    }

    public Deck() {
        cards = new ArrayList<>();
        hero = new Hero();
    }

    public void addCard(Card card) {
        cards.add(card);
    }

    public List<Card> getCards() {
        return cards;
    }

    public Hero getHero() {
        return hero;
    }

    public void setCards(List<Card> cards) {
        this.cards = cards;
    }

    public void setHero(Hero hero) {
        this.hero = hero;
    }

    public boolean isCardsNull() {
        return cards == null;
    }

    public String getTexturePath() {
        return texturePath;
    }

    public String getName() {
        return name;
    }

    public void setTexturePath(String texturePath) {
        this.texturePath = texturePath;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumberGame() {
        return numberGame;
    }

    public int getNumberWin() {
        return numberWin;
    }

    public void setNumberGame(int numberGame) {
        this.numberGame = numberGame;
    }

    public void setNumberWin(int numberWin) {
        this.numberWin = numberWin;
    }

    public boolean isNullHeroClass() {
        return hero.getHeroClass() == null;
    }

    public boolean isNullTexturePath() {
        return texturePath == null;
    }

    @Override
    public String toString() {
        float a;
        if (numberGame == 0) a = 0;
        else a = (float) numberWin / (float) numberGame;
        String s = "";
        s += "name: " + name + " / ";
        s += "all win divided by all game: " + a + " / ";
        s += "all win: " + numberWin + " / ";
        s += "all games: "+ numberGame + " / ";
        s += "mean of cards mana: " + StatusLogic.meanOfMamaDeck(this) + " / ";
        s += "hero: " + hero.getHeroClass() + " / ";
        s += "the most played card: " + StatusLogic.mostPlayedCard(this);
        return s;
    }
}
