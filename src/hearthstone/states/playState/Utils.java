package hearthstone.states.playState;

import Entity.cards.Card;
import hearthstone.configs.PlayStateConfigs;
import hearthstone.constants.Constants;
import hearthstone.ui.UICardImage;
import hearthstone.ui.UIDoubleImage;
import hearthstone.ui.UIObject;
import hearthstone.ui.UIRecImage;

import java.util.ArrayList;
import java.util.Calendar;

public class Utils {
    private static PlayStateConfigs p = PlayStateConfigs.getInstance();

    public static ArrayList<UIDoubleImage> findUIDoubleImage(ArrayList<UIDoubleImage> uiDoubleImages, ArrayList<Card> cards) {
        ArrayList<UIDoubleImage> uiObjects = new ArrayList<>(uiDoubleImages);
        ArrayList<UIDoubleImage> manager = new ArrayList<>();
        for (Card card : cards) {
            for (UIDoubleImage uiDoubleImage : uiObjects) {
                if (card.equals(uiDoubleImage.getCard())) {
                    manager.add(uiDoubleImage);
                    uiObjects.remove(uiDoubleImage);
                    break;
                }
            }
        }
        return manager;
    }

    public static ArrayList<UICardImage> findUICardImage(ArrayList<UICardImage> uiCardImages, ArrayList<Card> cards) {
        ArrayList<UICardImage> uiObjects = new ArrayList<>(uiCardImages);
        ArrayList<UICardImage> manager = new ArrayList<>();
        for (Card card : cards) {
            for (UICardImage uiCardImage : uiObjects) {
                if (card.equals(uiCardImage.getCard())) {
                    manager.add(uiCardImage);
                    uiObjects.remove(uiCardImage);
                    break;
                }
            }
        }
        return manager;
    }

    public static void arrangeCardDeck(ArrayList<UIDoubleImage> uiDoubleImages) {
        int cardWidth = 0;
        if (uiDoubleImages.size() != 0)
            cardWidth = p.get("maxWidthDeck") / Math.min(uiDoubleImages.size(), 6) - (3 * uiDoubleImages.size() + 1);
        cardWidth = Math.min(cardWidth, p.get("maxWidthCard"));
        int height = (int) (cardWidth * Constants.HEIGHT_DIVIDED_BY_WIDTH);
        float y = (float) ((p.get("maxHeightDeck") - height) / 2.0 + p.get("minInitialYPos"));
        float x = (float) (Constants.COMPUTER_WIDTH / 2.0 - (uiDoubleImages.size() / 2.0) * (cardWidth + 2));
        float x2 = (float) (Constants.COMPUTER_WIDTH / 2.0 - (uiDoubleImages.size() / 2.0) * (cardWidth + 2) - cardWidth / 2);
        float y2 = y - p.get("heightCardBig");
        for (UIDoubleImage uiDoubleImage : uiDoubleImages) {
            uiDoubleImage.setX(x);
            uiDoubleImage.setY(y);
            uiDoubleImage.setX2(x2);
            uiDoubleImage.setY2(y2);
            uiDoubleImage.setWidth(cardWidth);
            uiDoubleImage.setHeight(height);
            x += cardWidth + 5;
            x2 += cardWidth + 5;
        }
    }

    public static void arrangeCardLand(ArrayList<UICardImage> uiCardImages) {
        int height = p.get("maxHeightLand");
        int width = (int) (height / Constants.HEIGHT_DIVIDED_BY_WIDTH);
        float y = p.get("minInitialYPosLand");
        float x = (float) (Constants.COMPUTER_WIDTH / 2.0 - (uiCardImages.size() / 2.0) * (width + 2));
        for (UICardImage uiCardImage : uiCardImages) {
            uiCardImage.setX(x);
            uiCardImage.setY(y);
            uiCardImage.setHeight(height);
            uiCardImage.setWidth(width);
            x += width + 3;
        }
    }

}
