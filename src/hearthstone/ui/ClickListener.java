package hearthstone.ui;

import java.io.IOException;

public interface ClickListener {
    public void onClick();
}
