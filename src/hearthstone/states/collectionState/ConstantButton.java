package hearthstone.states.collectionState;

import Entity.HeroClass;
import Entity.cards.Deck;
import Entity.MainPlayer;
import data.Log;
import hearthstone.Handler;
import hearthstone.configs.CollectionStateConfigs;
import hearthstone.configs.MenuStateConfigs;
import hearthstone.gfx.Asserts;
import hearthstone.gfx.Input;
import hearthstone.states.State;
import hearthstone.states.infoPassive.InfoPassive;
import hearthstone.states.playState.PlayState;
import hearthstone.ui.ClickListener;
import hearthstone.ui.UIManager;
import hearthstone.ui.UIRecImage;
import hearthstone.utils.Util;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class ConstantButton {
    private static CollectionStateConfigs c = CollectionStateConfigs.getInstance();
    private static MenuStateConfigs m = MenuStateConfigs.getInstance();
    private UIManager uiManager;
    private Handler handler;
    private PlayState playState;
    private InfoPassive infoPassive;
    private Input searchInput;

    public ConstantButton(Handler handler) {
        this.handler = handler;
        uiManager = new UIManager(handler);
        searchInput = new Input(handler, 0, 0, 0, 30);
        heroButton();
        lockButton();
        unLockButton();
        manaButton();
        nextPageButton();
        previousPageButton();
        newDeck();
        back();
        done();
        delete();
        changeHero();
        Util.exit(uiManager);
        menu();
        play();
        search();
        searchIcon();
    }

    public void render(Graphics2D g2D) {
        uiManager.render(g2D);
        if (searchInput != null)
            searchInput.render(g2D);
    }

    public void tick() {
        uiManager.tick();
        if (searchInput != null)
            searchInput.tick();
    }

    private void updateCardInit() {
        try {
            handler.getCollectionState().cardInit();
        } catch (IOException e) {
            e.getStackTrace();
        }
    }

    private void heroButton() {
        uiManager.addButton(new UIRecImage(c.get("initialXPosHeroButton"), c.get("initialYPosHeroButton")
                , c.get("widthHeroButton"), c.get("heightHeroButton"), Asserts.neutral, () -> {
            try {
                Log.body("click hero button", "choose natural");
                handler.getCollectionState().setHeroClass(HeroClass.NATURAL);
                handler.getCollectionState().cardInit();
            } catch (IOException e) {
                e.getStackTrace();
            }
        }));
        for (int i = 0; i < c.get("heroNumber"); i++) {
            int finalI = i;
            uiManager.addButton(new UIRecImage(c.get("widthHeroButton") * (i + 1) + c.get("initialXPosHeroButton")
                    + c.get("horizontalDistanceHeroButton") * (i + 1), c.get("initialYPosHeroButton"),
                    c.get("widthHeroButton"), c.get("heightHeroButton"), Asserts.heroes[i], () -> {
                try {
                    Log.body("click hero button", "choose " + HeroNameInitUtils.recognizeHero(finalI));
                    handler.getCollectionState().setHeroClass(HeroNameInitUtils.recognizeHero(finalI));
                    handler.getCollectionState().cardInit();
                } catch (IOException e) {
                    e.getStackTrace();
                }
            }));
        }
    }

    private void lockButton() {
        uiManager.addButton(new UIRecImage(c.get("initialXPosLockButton"), c.get("initialYPosLockButton"),
                c.get("widthLockButton"), c.get("heightLockButton"), Asserts.lock, () -> {
            Log.body("click lock button", "show lock cards");
            handler.getCollectionState().setShowLockCards(!handler.getCollectionState().isShowLockCards());
            handler.getCollectionState().setShowUnLockCards(false);
            updateCardInit();
        }));

    }

    private void unLockButton() {
        uiManager.addButton(new UIRecImage(c.get("initialXPosLockButton") + c.get("widthLockButton") +
                c.get("horizontalDistanceLockButton"), c.get("initialYPosLockButton"),
                c.get("widthLockButton"), c.get("heightLockButton"), Asserts.unlock, () -> {
            Log.body("click unlock button", "show unlock cards");
            handler.getCollectionState().setShowLockCards(false);
            handler.getCollectionState().setShowUnLockCards(!handler.getCollectionState().isShowUnLockCards());
            updateCardInit();
        }));
    }

    private void manaButton() {
        int x = 0;
        uiManager.addButton(new UIRecImage(c.get("initialXPosManaButton"), c.get("initialYPosManaButton")
                , c.get("widthManaButton"), c.get("heightManaButton"), Asserts.mana[0], () -> {
            Log.body("click back", "reject mana filter");
            handler.getCollectionState().setMana(-1);
            updateCardInit();
        }));
        x += c.get("widthManaButton") + c.get("horizontalDistanceManaButton");
        for (int i = 1; i < 11; i++) {
            int finalI = i;
            uiManager.addButton(new UIRecImage(c.get("initialXPosManaButton") + x, c.get("initialYPosManaButton")
                    , c.get("widthManaButton"), c.get("heightManaButton"), Asserts.mana[i], () -> {
                Log.body("click mana filter", "show cards with " + finalI + " mana");
                handler.getCollectionState().setMana(finalI);
                updateCardInit();
            }));
            x += c.get("widthManaButton") + c.get("horizontalDistanceManaButton");
        }
    }

    private void nextPageButton() {
        uiManager.addButton(new UIRecImage(1150, 380, 20, 20, Asserts.nextPage, () -> {
            try {
                Log.body("click page button", "change page");
                if (handler.getCollectionState().getPage() == 1)
                    handler.getCollectionState().setPage(2);
                else if (handler.getCollectionState().getPage() == 2)
                    handler.getCollectionState().setPage(3);
                handler.getCollectionState().cardInit();
            } catch (IOException e) {
                e.getStackTrace();
            }
        }));
    }

    private void previousPageButton() {
        uiManager.addButton(new UIRecImage(10, 380, 20, 20, Asserts.nextPage, () -> {
            try {
                Log.body("click page button", "change page");
                if (handler.getCollectionState().getPage() == 3)
                    handler.getCollectionState().setPage(2);
                else if (handler.getCollectionState().getPage() == 2)
                    handler.getCollectionState().setPage(1);
                handler.getCollectionState().cardInit();
            } catch (IOException e) {
                e.getStackTrace();
            }
        }));
    }

    private void newDeck() {
        uiManager.addButton(new UIRecImage(c.get("initialXPosDeckButton") + c.get("horizontalDistanceDeckButton"),
                c.get("initialYPosDeckButton") + c.get("deckNumber") * c.get("heightDeckButton") +
                        (c.get("deckNumber") + 1) * c.get("verticalDistanceDeckButton"), c.get("widthDeckButton"),
                c.get("heightDeckButton"), Asserts.newDeck, () -> {
            for (int i = 0; i < MainPlayer.getInstance().getDecks().length; i++) {
                if (MainPlayer.getInstance().getDecks()[i] == null) {
                    if (handler.getCollectionState().getCurrentDeck() == null) {
                        Log.body("click new deck button", "create a new deck");
                        handler.getCollectionState().setCurrentDeck(new Deck());
                        handler.getCollectionState().heroNameButtonInit();
                        handler.getCollectionState().setDeckIndex(i);
                        updateCardInit();
                    } else {
                        CollectionState.setError(true);
                        CollectionState.setErrorMassage("You can not create a new deck at the moment!");
                        Log.body("error in collection state:click new deck button", "You can not create a new deck at the moment!");
                    }
                    break;
                }
            }
        }));
    }

    private void back() {
        int y = c.get("initialYPosDeckButton") + (c.get("deckNumber") + 1) * c.get("heightDeckButton") +
                (c.get("deckNumber") + 2) * c.get("verticalDistanceDeckButton");
        uiManager.addButton(new UIRecImage(c.get("initialXPosDeckButton") + c.get("horizontalDistanceDeckButton"),
                y, (c.get("widthDeckButton") - 1) / 2, c.get("heightDeckButton"), Asserts.back, () -> {
            Log.body("click back button", "back to decks");
            handler.getCollectionState().setSearchInput(new Input(handler, 0, 0, 0, 0));
            handler.getCollectionState().getContainWriteHeroName().get(0).setImages(Asserts.writeName);
            handler.getCollectionState().setCurrentDeck(null);
            handler.getCollectionState().deckButtonInit();
            updateCardInit();
        }));
    }

    private void delete() {
        int y = c.get("initialYPosDeckButton") + (c.get("deckNumber") + 2) * c.get("heightDeckButton") +
                (c.get("deckNumber") + 3) * c.get("verticalDistanceDeckButton");
        uiManager.addButton(new UIRecImage(c.get("initialXPosDeckButton") + c.get("horizontalDistanceDeckButton"),
                y, (c.get("widthDeckButton") - 1) / 2, c.get("heightDeckButton"), Asserts.delete, () -> {
            if (handler.getCollectionState().getCurrentDeck() != null) {
                Log.body("click delete button", "delete the current deck");
                handler.getCollectionState().setSearchInput(new Input(handler, 0, 0, 0, 0));
                handler.getCollectionState().getContainWriteHeroName().get(0).setImages(Asserts.writeName);
                handler.getCollectionState().setCurrentDeck(null);
                if (MainPlayer.getInstance().getDecks()[handler.getCollectionState().getDeckIndex()] != null) {
                    Utils.changeIndex(MainPlayer.getInstance().getDecks(), handler.getCollectionState().getDeckIndex());
                    handler.getCollectionState().setDeckPictureInit();
                }
                handler.getCollectionState().deckButtonInit();
                updateCardInit();
            } else {
                CollectionState.setError(true);
                CollectionState.setErrorMassage("You have not choose any decks for deleting!");
                Log.body("error in collection state: click delete button", "You have not choose any decks for deleting!");
            }
        }));
    }

    private void done() {
        int y = c.get("initialYPosDeckButton") + (c.get("deckNumber") + 1) * c.get("heightDeckButton") +
                (c.get("deckNumber") + 2) * c.get("verticalDistanceDeckButton");
        uiManager.addButton(new UIRecImage(c.get("initialXPosDeckButton") + c.get("horizontalDistanceDeckButton") +
                (float) ((c.get("widthDeckButton") + 1) / 2.0), y, (c.get("widthDeckButton")) / 2,
                c.get("heightDeckButton"), Asserts.done, () -> {
            if (handler.getCollectionState().getCurrentDeck() == null) {
                CollectionState.setError(true);
                CollectionState.setErrorMassage("You have not choose any decks!");
                Log.body("error in collection state: click done button", "You have not choose any decks!");
            } else {
                if (!handler.getCollectionState().getSearchInput().getInput().equals("")) {
                    CollectionState.setError(true);
                    CollectionState.setErrorMassage("Write a name for your deck!");
                    Log.body("error in collection state: click done button", "Write a name for your deck!");
                } else {
                    if (handler.getCollectionState().getCurrentDeck().isCardsNull()) {
                        CollectionState.setError(true);
                        CollectionState.setErrorMassage("You have not choose any cards!");
                        Log.body("error in collection state: click done button", "You have not choose any cards!");
                    } else {
                        if (handler.getCollectionState().getCurrentDeck().getCards().size() != 15) {
                            CollectionState.setError(true);
                            CollectionState.setErrorMassage("You must have exactly 15 cards!");
                            Log.body("error in collection stateL: click done button", "You must have exactly 15 cards!");
                        } else {
                            MainPlayer.getInstance().setDeck(handler.getCollectionState().getCurrentDeck(), handler.getCollectionState().getDeckIndex());
                            Log.body("done button click", "create a deck successfully");
                            handler.getCollectionState().setDeckPicture();
                            handler.getCollectionState().deckButtonInit();
                            handler.getCollectionState().setCurrentDeck(null);
                            updateCardInit();
                        }
                    }
                }
            }
        }));
    }

    private void changeHero() {
        int y = c.get("initialYPosDeckButton") + (c.get("deckNumber") + 2) * c.get("heightDeckButton") +
                (c.get("deckNumber") + 3) * c.get("verticalDistanceDeckButton");
        uiManager.addButton(new UIRecImage(c.get("initialXPosDeckButton") + c.get("horizontalDistanceDeckButton") +
                (float) ((c.get("widthDeckButton") + 1) / 2.0), y, (c.get("widthDeckButton")) / 2,
                c.get("heightDeckButton"), Asserts.changeHero, () -> {
            if (handler.getCollectionState().getCurrentDeck() != null && !handler.getCollectionState().getCurrentDeck().isNullHeroClass() && handler.getCollectionState().getSearchInput().getInput().equals("")) {
                Log.body("change hero button", "change the hero of " + handler.getCollectionState().getCurrentDeck().getName());
                handler.getCollectionState().heroNameButtonInit();
            } else if (handler.getCollectionState().getCurrentDeck() == null) {
                CollectionState.setError(true);
                CollectionState.setErrorMassage("You have not choose any decks!");
                Log.body("error in collection state: click change hero", "You have not choose any decks!");
            } else if (handler.getCollectionState().getCurrentDeck().isNullHeroClass()) {
                CollectionState.setError(true);
                CollectionState.setErrorMassage("You do not have any heroes for changing!");
                Log.body("error in collection state: click change hero", "You do not have any heroes for changing!");
            } else if (!handler.getCollectionState().getSearchInput().getInput().equals("")) {
                CollectionState.setError(true);
                CollectionState.setErrorMassage("Write a name for your deck!");
                Log.body("error in collection state: click change hero", "Write a name for your deck!");
            }
        }));
    }

    private void menu() {
        uiManager.addButton(new UIRecImage(m.getMenuSateConfigs("initialXPosMenu"), m.getMenuSateConfigs("initialYPosMenu"),
                m.getMenuSateConfigs("widthMenu"), m.getMenuSateConfigs("heightMenu"), Asserts.menu, () -> {
            Log.body("click menu button", "back to menu sate");
            handler.getCollectionState().setCurrentDeck(null);
            State.setCurrentState(handler.getMenuState());
            handler.getCollectionState().setPage(1);
        }));
    }

    private void play() {
        uiManager.addButton(new UIRecImage(c.get("initialXPosPlay"), c.get("initialYPosPlay"), c.get("widthPlay"),
                c.get("heightPlay"), Asserts.playInCollection, () -> {
            if (handler.getCollectionState().getCurrentDeck() != null && !handler.getCollectionState().getCurrentDeck().isNullHeroClass() &&
                    handler.getCollectionState().getCurrentDeck().getCards().size() == 15 && handler.getCollectionState().getSearchInput().getInput().equals("")) {
                Log.body("click play", "start playing from collection state");
                handler.getCollectionState().setPlayDeck(handler.getCollectionState().getCurrentDeck());
                handler.getCollectionState().setCurrentDeck(null);
                infoPassive = new InfoPassive(handler);
                handler.setInfoPassive(infoPassive);
                State.setCurrentState(infoPassive);
                handler.getCollectionState().deckButtonInit();
            } else if (handler.getCollectionState().getCurrentDeck() == null) {
                CollectionState.setError(true);
                CollectionState.setErrorMassage("You have not choose any decks!");
                Log.body("error in collection state: click play", "You have not choose any decks!");
            } else if (handler.getCollectionState().getCurrentDeck().getCards().size() != 15) {
                CollectionState.setError(true);
                CollectionState.setErrorMassage("Complete Your deck!");
                Log.body("error in collection state: click play", "Complete Your deck!");
            } else if (handler.getCollectionState().getCurrentDeck().isNullHeroClass()) {
                CollectionState.setError(true);
                CollectionState.setErrorMassage("Choose a hero!");
                Log.body("error in collection state: click play", "Choose a hero!");
            }
        }));
    }

    private void search() {
        uiManager.addButton(new UIRecImage(c.get("initialXPosSearch"), c.get("initialYPosSearch"), c.get("widthSearch"),
                c.get("heightSearch"), Asserts.search, new ClickListener() {
            @Override
            public void onClick() {
                Log.body("click search", "write for searching");
                BufferedImage[] bufferedImages = new BufferedImage[]{Asserts.search[1], Asserts.search[1]};
                uiManager.getButtons().get(uiManager.getButtons().size() - 2).setImages(bufferedImages);
                searchInput = new Input(handler, 15, c.get("initialXPosSearchString"), c.get("initialYPosSearchString"), 30);
            }
        }));

    }

    private void searchIcon() {
        uiManager.addButton(new UIRecImage(c.get("initialXPosSearchIcon"), c.get("initialYPosSearchIcon"),
                c.get("widthSearchIcon"), c.get("heightSearchIcon"), Asserts.searchIcon, new ClickListener() {
            @Override
            public void onClick() {
                Log.body("click searchIcon", "click for searching");
                uiManager.getButtons().get(uiManager.getButtons().size() - 2).setImages(Asserts.search);
                updateCardInit();
                searchInput = new Input(handler, 0, 0, 0, 30);
            }
        }));
    }

    public Input getSearchInput() {
        return searchInput;
    }
}
