package hearthstone;

import hearthstone.configs.ConfigLoader;
import hearthstone.login.LoginFrame;
import hearthstone.login.LoginPanel;

import javax.swing.*;

public class Launcher {
    private static Game game;
    public static void main(String[] args) {
        Launcher launcher = new Launcher(args);
    }

    public Launcher(String[] args){
        ConfigLoader urls = ConfigLoader.getInstance(getConfigFile(args));
        LoginFrame loginFrame = new LoginFrame();

    }
    private String getConfigFile(String[] args){
        String configAddress = "default";
        if(args.length > 1){
            configAddress = args[1];
        }
        return configAddress;
    }

    public static Game getGame() {
        return game;
    }

    public static void setGame(Game game) {
        Launcher.game = game;
    }
}

