package Entity.cards;

import Entity.HeroClass;

import java.awt.image.BufferedImage;

public class Minion extends Card{
    private Integer HP;
    private Integer attack;

    public Minion(String name, int cost, int mana, Rarity rarity, HeroClass heroClass, Type type, String description, Integer HP, Integer attack, String[] texturePath, boolean lock) {
        super(name ,cost, mana, rarity, heroClass, type, description, texturePath, lock);
        this.HP = HP;
        this.attack = attack;
    }
}
