package Entity.cards;

import Entity.HeroClass;

import java.awt.image.BufferedImage;
import java.util.Comparator;
import java.util.Objects;

public class Card implements Comparable<Card> {
    private String name;
    private int cost;
    private int mana;
    private Rarity rarity;
    private HeroClass heroClass;
    private Type type;
    private String description;
    private String[] texturePath;
    private boolean lock;
    private int numberDraw;

    public Card(String name, int cost, int mana, Rarity rarity, HeroClass heroClass, Type type, String description, String[] texturePath, boolean lock) {
        this.name = name;
        this.cost = cost;
        this.mana = mana;
        this.rarity = rarity;
        this.heroClass = heroClass;
        this.type = type;
        this.description = description;
        this.texturePath = texturePath;
        this.lock = lock;

    }

    public String getName() {
        return name;
    }

    public HeroClass getHeroClass() {
        return heroClass;
    }

    public int getCost() {
        return cost;
    }

    public int getMana() {
        return mana;
    }

    public Rarity getRarity() {
        return rarity;
    }

    public Type getType() {
        return type;
    }

    public String getDescription() {
        return description;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public void setMana(int mana) {
        this.mana = mana;
    }

    public void setRarity(Rarity rarity) {
        this.rarity = rarity;
    }

    public void setHeroClass(HeroClass heroClass) {
        this.heroClass = heroClass;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isLock() {
        return lock;
    }

    public void setLock(boolean lock) {
        this.lock = lock;
    }

    public String[] getTexturePath() {
        return texturePath;
    }

    public void setTexturePath(String[] texturePath) {
        this.texturePath = texturePath;
    }

    public int getNumberDraw() {
        return numberDraw;
    }

    public void setNumberDraw(int numberDraw) {
        this.numberDraw = numberDraw;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Card card = (Card) o;
        return name.equals(card.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }


    @Override
    public int compareTo(Card card) {
        return Integer.compare(this.getMana(), card.getMana());
    }


}

