package hearthstone.ui;

import Entity.cards.Card;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Comparator;
import java.util.Objects;

public class UIDoubleImage extends UIObject implements Comparable<UIDoubleImage> {
    private float x2, y2;
    private int width2, height2;
    private Card card;

    public UIDoubleImage(Card card, float x, float y, float x2, float y2, int width, int height, int width2, int height2, BufferedImage[] images, ClickListener clicker) {
        super(x, y, width, height, images, clicker);
        this.x2 = x2;
        this.y2 = y2;
        this.height2 = height2;
        this.width2 = width2;
        this.card = card;
    }

//    @Override
//    public void tick() {}

    @Override
    public void render(Graphics2D g2D) {
        if (hovering) {
            g2D.drawImage(images[1], (int) x2, (int) y2, width2, height2, null);
            g2D.drawImage(images[0], (int)x, (int)y, width, height, null);
        }
        else
            g2D.drawImage(images[0], (int)x, (int)y, width, height, null);
    }

    @Override
    public void onClick() {
        clicker.onClick();
    }

    public Card getCard() {
        return card;
    }

    public void setX2(float x2) {
        this.x2 = x2;
    }

    public void setY2(float y2) {
        this.y2 = y2;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UIDoubleImage that = (UIDoubleImage) o;
        return card.equals(that.card);
    }

    @Override
    public int hashCode() {
        return Objects.hash(card);
    }

    @Override
    public int compareTo(UIDoubleImage uiDoubleImage) {
        return Integer.compare(this.card.getMana(), uiDoubleImage.getCard().getMana());
    }
}
