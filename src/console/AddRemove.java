package console;

import Entity.cards.Card;
import com.google.gson.Gson;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;
import java.util.Scanner;

public class AddRemove {

    public boolean addRemoveCard(String cardName, List<Card> cards, String addOrRemove) throws FileNotFoundException {
        Scanner fileReader = new Scanner(new FileReader("cards.txt"));
        while (fileReader.hasNext()) {
            Card card = new Gson().fromJson(fileReader.nextLine(), Card.class);
            if (card.getName().equals(cardName)) {
                if (addOrRemove.equals("add")) {
                    cards.add(card);
                    fileReader.close();
                    return true;
                } else if (addOrRemove.equals("remove")) {
                    for (int i = 0; i < cards.size(); i++) {
                        if (card.getName().equals(cards.get(i).getName())) {
                            cards.remove(i);
                            break;
                        }
                    }
                    fileReader.close();
                    return true;
                }
            }
        }
        return false;
    }
}