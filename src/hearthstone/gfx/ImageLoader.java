package hearthstone.gfx;

import Entity.MainPlayer;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class ImageLoader {

    public static BufferedImage loadImage(String path) {
        try {
            return ImageIO.read(ImageLoader.class.getResource(path));
        } catch (IOException e) {
            e.getStackTrace();
        }
        return null;
    }

    public static BufferedImage[] loadDoubleImage(String path1, String path2) {
        try {
            return new BufferedImage[]{ImageIO.read(ImageLoader.class.getResource(path1)),
                    ImageIO.read(ImageLoader.class.getResource(path2))};
        } catch (IOException e) {
            e.getStackTrace();
        }
        return null;
    }

    public static void writeOnImage(BufferedImage bufferedImage, String name) {
        ImageLoader.copyImage(bufferedImage, "res/texture/new hero picture/" + name + MainPlayer.getInstance().getUserId() + ".png");
        BufferedImage b = ImageLoader.loadImage("/texture/new hero picture/" + name + MainPlayer.getInstance().getUserId() + ".png");
        Graphics g = b.getGraphics();
        Font font = new Font("Helvetica", Font.BOLD, 100);
        FontMetrics fontMetrics = g.getFontMetrics(font);
        int width = fontMetrics.stringWidth(name);
        g.setFont(font);
        g.setColor(Color.WHITE);
        g.drawString(name, (b.getWidth() - width) / 2, (b.getHeight() + 50) / 2);
        g.dispose();
        try {
            ImageIO.write(b, "png", new File("res/texture/new hero picture/" + name + MainPlayer.getInstance().getUserId() + ".png"));
        } catch (IOException e) {
            e.getStackTrace();
        }
    }

    public static void copyImage(BufferedImage bufferedImage, String path) {
        try {
            ImageIO.write(bufferedImage, "png", new File(path));
        } catch (IOException e) {
            e.getStackTrace();
        }
    }
}
