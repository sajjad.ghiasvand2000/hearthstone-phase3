package Entity.cards;

import Entity.HeroClass;

import java.awt.image.BufferedImage;

public class Weapon extends Card {
    private Integer durability;
    private Integer attack;

    public Weapon(String name, int cost, int mana, Rarity rarity, HeroClass heroClass, Type type, String description, Integer durability, Integer attack, String[] texturePath, boolean lock) {
        super(name, cost, mana, rarity, heroClass, type, description, texturePath, lock );
        this.durability = durability;
        this.attack = attack;
    }
}
