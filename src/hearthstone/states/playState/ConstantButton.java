package hearthstone.states.playState;

import data.Log;
import hearthstone.Handler;
import hearthstone.configs.MenuStateConfigs;
import hearthstone.configs.PlayStateConfigs;
import hearthstone.gfx.Asserts;
import hearthstone.gfx.Drawer;
import hearthstone.states.State;
import hearthstone.ui.UIManager;
import hearthstone.ui.UIRecImage;
import hearthstone.utils.Util;

import java.awt.*;

public class ConstantButton {
    private PlayStateConfigs p = PlayStateConfigs.getInstance();
    private static MenuStateConfigs m = MenuStateConfigs.getInstance();
    private Handler handler;
    private UIManager uiManager;
    private GamePlayer gamePlayer;
    private int deckIndex = 3;
    private boolean twiceDrawPassive;

    public ConstantButton(Handler handler) {
        this.handler = handler;
        uiManager = new UIManager(handler);
        twiceDrawPassive = handler.getInfoPassive().isTwiceDrawPassive();
        menu();
        Util.exit(uiManager);
        endTurn();
    }

    public void tick() {
        uiManager.tick();
    }

    public void render(Graphics2D g2D) {
        uiManager.render(g2D);
        Drawer.numberCardInDeck("Number of cards: " + (15 - deckIndex), g2D);
    }

    private void menu() {
        uiManager.addButton(new UIRecImage(m.getMenuSateConfigs("initialXPosMenu"), m.getMenuSateConfigs("initialYPosMenu"),
                m.getMenuSateConfigs("widthMenu"), m.getMenuSateConfigs("heightMenu"), Asserts.menu, () -> {
            Log.body("click menu button", "back to menu sate");
            State.setCurrentState(handler.getMenuState());
        }));
    }

    private void endTurn() {
        uiManager.addButton(new UIRecImage(p.get("initialXPos"), p.get("initialYPos"), p.get("widthEndTurn"),
                p.get("heightEndTurn"), Asserts.endTurn, () -> {
            Log.body("click end turn", "click end turn in play state");
            gamePlayer = handler.getPlayState().getGamePlayer();
            gamePlayer.setLevel(gamePlayer.getLevel() + 1);
            gamePlayer.setMana(Math.min(gamePlayer.getLevel(), 10));
            addCardToDeck();
            gamePlayer.cardDeckInit();
            gamePlayer.cardDeckInit();
        }));
    }

    private void addCardToDeck() {
            if (deckIndex <= 14 && gamePlayer.getContainDeckCard().size() < 12) {
                gamePlayer.getContainDeckCard().add(gamePlayer.getDeck().getCards().get(deckIndex));
                deckIndex++;
                if (twiceDrawPassive){
                    if (deckIndex <= 14 && gamePlayer.getContainDeckCard().size() < 12){
                        gamePlayer.getContainDeckCard().add(gamePlayer.getDeck().getCards().get(deckIndex));
                        deckIndex++;
                    }
                }
            }
    }
}
