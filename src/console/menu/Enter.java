//package console.menu;
//
//import Entity.MainPlayer;
//import console.State;
//import data.Log;
//
//import java.io.IOException;
//import java.util.Scanner;
//
//public class Enter {
//
//    public static State setState() throws IOException {
//        Scanner scanner = new Scanner(System.in);
//        MainPlayer m = MainPlayer.getInstance();
//        System.out.println("Choose one of these:\nexit\nexit-a\ndelete-player\ncollections\nstore\nhearthstone--help\nback");
//        String next = scanner.nextLine();
//        switch (next) {
//            case "exit":
//                Log.body(m.getUserId(), m.getUsername(), "exit", "exit");
//                return State.preLogin;
//            case "exit-a":
//                Log.body(m.getUserId(), m.getUsername(), "exit", "exit and stop");
//                System.exit(0);
//                return null;
//            case "delete-player":
//                return State.deletePlayer;
//            case "collections":
//                Log.body(m.getUserId(), m.getUsername(), "navigate", "collections");
//                return State.collections;
//            case "store":
//                Log.body(m.getUserId(), m.getUsername(), "navigate", "store");
//                return State.store;
//            case "back":
//                Log.body(m.getUserId(), m.getUsername(), "back", "return to preLogin");
//                return State.preLogin;
//            case "hearthstone--help":
//                System.out.println("exit:If you want to go to preLogin\nexit-a:If you want to exit.");
//                System.out.println("delete-player:If you want to delete your account.\ncollections:If you want to manage your cards.");
//                System.out.println("store:If you want buy or sell cards\nhearthstone--help\nback");
//                System.out.println("-----------------------------------------------");
//                Log.body(m.getUserId(), m.getUsername(), "help", "hearthstone help");
//                return State.enter;
//            default:
//                System.out.println("Inputs are incorrect please try again.");
//                Log.body(m.getUserId(), m.getUsername(), "error", "wrong inputs in menu");
//                return State.enter;
//        }
//    }
//}
