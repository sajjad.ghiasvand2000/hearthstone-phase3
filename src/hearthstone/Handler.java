package hearthstone;

import hearthstone.display.Display;
import hearthstone.input.KeyManager;
import hearthstone.input.MouseManager;
import hearthstone.states.infoPassive.InfoPassive;
import hearthstone.states.menuState.MenuState;
import hearthstone.states.collectionState.CollectionState;
import hearthstone.states.playState.PlayState;
import hearthstone.states.shopState.ShopState;
import hearthstone.states.statusState.graphic.StatusState;

public class Handler {
    private Game game;

    public Handler(Game game) {
        this.game = game;
    }

    public Display getDisplay(){
        return game.getDisplay();
    }

    public CollectionState getCollectionState(){return game.getCollectionState();}

    public ShopState getShopState(){return game.getShopState();}
    public MenuState getMenuState(){return game.getMenuState();}
    public PlayState getPlayState(){return game.getPlayState();}
    public InfoPassive getInfoPassive(){return game.getInfoPassive();}
    public StatusState getStatusState(){return game.getStatusState();}
    public void setPlayState(PlayState playState){ game.setPlayState(playState);}
    public void setInfoPassive(InfoPassive infoPassive){ game.setInfoPassive(infoPassive);}
    public void setStatusState(StatusState statusState){ game.setStatusState(statusState);}
    public MouseManager getMouseManager(){return game.getMouseManager();}
    public KeyManager getKeyManager(){return game.getKeyManager();}

}
