//package console;
//
//import console.card.LsACards;
//import console.card.LsMCards;
//import console.card.LsNCards;
//import console.card.SelectCard;
//import console.collections.Collections;
//import console.collections.LsAHeroes;
//import console.collections.LsMHeroes;
//import console.login.Login;
//import console.login.PreLogin;
//import console.login.SignUp;
//import console.menu.DeletePlayer;
//import console.menu.Enter;
//import console.store.LsB;
//import console.store.LsS;
//import console.store.Store;
//
//import java.io.IOException;
//
//public class CommandLine {
//    private State state;
//
//    public CommandLine() {
//        state = State.preLogin;
//    }
//
//    public void run() throws IOException{
//        while (true) {
//
//            switch (state) {
//                case preLogin:
//                    state = PreLogin.setState();
//                    break;
//
//                case login:
//                    state = Login.setState();
//                    break;
//
//                case signUp:
//                    state = SignUp.setState();
//                    break;
//
//                case enter:
//                    state = Enter.setState();
//                    break;
//
//                case deletePlayer:
//                    state = DeletePlayer.setState();
//                    break;
//
//                case collections:
//                    state = Collections.setState();
//                    break;
//
//                case ls_a_heroes:
//                    state = LsAHeroes.setState();
//                    break;
//
//                case ls_m_heroes:
//                    state = LsMHeroes.setState();
//                    break;
//
//                case selectCard:
//                    state = SelectCard.steState();
//                    break;
//
//                case ls_a_cards:
//                    state = LsACards.setstate();
//                    break;
//
//                case ls_m_cards:
//                    state = LsMCards.setState();
//                    break;
//
//                case ls_n_cards:
//                    state = LsNCards.setState();
//                    break;
//
//                case store:
//                    state = Store.setState();
//                    break;
//
//                case ls_b:
//                    state = LsB.setState();
//                    break;
//
//                case ls_s:
//                    state = LsS.setState();
//                    break;
//            }
//        }
//    }
//}
