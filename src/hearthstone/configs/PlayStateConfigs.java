package hearthstone.configs;

import hearthstone.states.playState.PlayState;

public class PlayStateConfigs{
    private String name;
    private Configs properties;
    private static PlayStateConfigs playStateConfigs;

    private PlayStateConfigs(String name){
        properties = ConfigLoader.getInstance().getPlayStateConfigs(name);
    }

    public static PlayStateConfigs getInstance(String name){
        if (playStateConfigs == null)
            playStateConfigs = new PlayStateConfigs(name);
        return playStateConfigs;
    }

    public static PlayStateConfigs getInstance(){
        return getInstance("PLAY_STATE_CONFIG_FILE");
    }

    public int get(String name){
        return properties.readInteger(name);
    }
}
