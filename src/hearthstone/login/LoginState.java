package hearthstone.login;

import Entity.Player;
import com.google.gson.Gson;
import Entity.MainPlayer;
import data.Log;
import hearthstone.Game;
import hearthstone.Launcher;

import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class LoginState {
    private String username;
    private String password;


    public LoginState(String username, String password) throws IOException {
        this.username = username;
        this.password = password;
        init();
    }

    private void init() throws IOException {
        Scanner fileReader = new Scanner(new FileReader("player.txt"));
        while (fileReader.hasNext()) {
            Player mainPlayer = new Gson().fromJson(fileReader.nextLine(), Player.class);
            if (username.equals(mainPlayer.getUsername()) && password.equals(mainPlayer.getPassword())) {
                MainPlayer.getInstance().setUserId(mainPlayer.getUserId());
                MainPlayer.getInstance().setUsername(mainPlayer.getUsername());
                MainPlayer.getInstance().setPassword(mainPlayer.getPassword());
                MainPlayer.getInstance().setHeroes(mainPlayer.getHeroes());
                MainPlayer.getInstance().setEntireCards(mainPlayer.getEntireCards());
                MainPlayer.getInstance().setDecks(mainPlayer.getDecks());
                MainPlayer.getInstance().setDiamond(mainPlayer.getDiamond());
                Log.body("login", "successful login");
                fileReader.close();
                LoginFrame.getTextArea().append("Congratulation!Your username and password were true.\n");
                Launcher.setGame(new Game(1700, 1000, "Hearthstone"));
                Launcher.getGame().start();
                return;
            }
        }
        fileReader.close();
        LoginFrame.getTextArea().append("You Have Entered Wrong Username Or Password.\nPlease Try Again.\n");
    }
}
