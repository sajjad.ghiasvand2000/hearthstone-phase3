package Entity;

import Entity.cards.Card;
import Entity.cards.Deck;
import console.AddRemove;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public class Player extends AddRemove {

    private String username;
    private String password;
    private int userId;
    private int diamond ;
    private ArrayList<Card> entireCards;
    private Deck[] decks;
    private List<Hero> heroes;

    public boolean addRemoveEntireCard(String cardName, String addOrRemove) throws FileNotFoundException {
        return addRemoveCard(cardName, entireCards, addOrRemove);
    }

//    public boolean addRemoveCurrentCard(String cardName, String addOrRemove) throws FileNotFoundException {
//        return addRemoveCard(cardName, currentCards, addOrRemove);
//    }

    public Player(String name, String password) {
        this.username = name;
        this.password = password;
    }

    public Player(String username, String password, List<Hero> heroes) {
        this.username = username;
        this.password = password;
        this.heroes = heroes;
    }

    public Player(String username, String password, ArrayList<Card> entireCards, Deck[] decks, List<Hero> heroes) {
        this.username = username;
        this.password = password;
        this.entireCards = entireCards;
        this.decks = decks;
        this.heroes = heroes;
    }

    public Player() {
        decks = new Deck[13];
    }

    public void setHeroes(List<Hero> heroes) {
        this.heroes = heroes;
    }

    public List<Hero> getHeroes() {
        return heroes;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public int getDiamond() {
        return diamond;
    }

    public void setUsername(String name) {
        this.username = name;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setDiamond(int diamond) {
        this.diamond = diamond;
    }

    public ArrayList<Card> getEntireCards() {
        return entireCards;
    }

    public Deck[] getDecks() {
        return decks;
    }

    public void setEntireCards(ArrayList<Card> entireCards) {
        this.entireCards = entireCards;
    }

    public void setDecks(Deck[] decks) {
        this.decks = decks;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public void setDeck(Deck deck, int indexDeck){
        decks[indexDeck] = deck;
    }

}
