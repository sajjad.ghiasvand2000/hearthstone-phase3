package Entity;

public enum HeroClass {
    MAGE,
    ROGUE,
    WARLOCK,
    NATURAL,
    HUNTER,
    PALADIN
}
