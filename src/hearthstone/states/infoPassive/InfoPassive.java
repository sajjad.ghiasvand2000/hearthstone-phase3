package hearthstone.states.infoPassive;

import data.Log;
import hearthstone.Handler;
import hearthstone.configs.PassiveStateConfigs;
import hearthstone.constants.Constants;
import hearthstone.gfx.Asserts;
import hearthstone.states.State;
import hearthstone.states.playState.PlayState;
import hearthstone.ui.ClickListener;
import hearthstone.ui.UIManager;
import hearthstone.ui.UIRecImage;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class InfoPassive extends State {
    private PassiveStateConfigs p = PassiveStateConfigs.getInstance();
    private ArrayList<UIRecImage> containPassive;
    private UIManager uiManager;
    private boolean twiceDrawPassive = false;
    private int offCardPassive = 0;
    private int ManaJumpPassive = 0;

    public InfoPassive(Handler handler) {
        super(handler);
        uiManager = new UIManager(handler);
        containPassive = new ArrayList<>();
        twiceDraw();
        offCard();
        ManaJump();
        warriors();
        freePower();
        passiveInit();
    }

    @Override
    public void tick() {
        uiManager.tick();
    }

    @Override
    public void render(Graphics2D g2D) {
        g2D.drawImage(Asserts.passiveBackground, 0, 0, Constants.COMPUTER_WIDTH, Constants.COMPUTER_HEIGHT, null);
        uiManager.render(g2D);
    }

    private void twiceDraw() {
        containPassive.add(new UIRecImage(0, 0, p.get("widthCard"), p.get("heightCard"), Asserts.passive[1], () -> {
            Log.body("info passive", "click twice draw");
            twiceDrawPassive = true;
            start();
        }));
    }

    private void offCard() {
        containPassive.add(new UIRecImage(0, 0, p.get("widthCard"), p.get("heightCard"), Asserts.passive[2], () -> {
            Log.body("info passive", "click off card");
            offCardPassive = 1;
            start();
        }));
    }

    private void ManaJump() {
        containPassive.add(new UIRecImage(0, 0, p.get("widthCard"), p.get("heightCard"), Asserts.passive[0], () -> {
            Log.body("info passive", "click manaJump");
            ManaJumpPassive = 1;
            start();
        }));
    }

    private void warriors() {
        containPassive.add(new UIRecImage(0, 0, p.get("widthCard"), p.get("heightCard"), Asserts.passive[4], () -> {
            Log.body("info passive", "click warriors");
            start();
        }));
    }

    private void freePower() {
        containPassive.add(new UIRecImage(0, 0, p.get("widthCard"), p.get("heightCard"), Asserts.passive[3], () -> {
            Log.body("info passive", "click free power");
            start();
        }));
    }

    private void start() {
        PlayState playState = new PlayState(handler);
        handler.setPlayState(playState);
        State.setCurrentState(handler.getPlayState());
    }

    private void passiveInit() {
        Collections.shuffle(containPassive);
        ArrayList<UIRecImage> manger = new ArrayList<>();
        Collections.addAll(manger, containPassive.get(0), containPassive.get(1), containPassive.get(2));
        Utils.arrange(manger);
        uiManager.setButtons(Utils.toUIObject(manger));

    }

    public boolean isTwiceDrawPassive() {
        return twiceDrawPassive;
    }

    public int getOffCardPassive() {
        return offCardPassive;
    }

    public int getManaJumpPassive() {
        return ManaJumpPassive;
    }
}
