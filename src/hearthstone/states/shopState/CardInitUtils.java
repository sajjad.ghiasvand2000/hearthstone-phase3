package hearthstone.states.shopState;

import Entity.cards.Card;
import Entity.cards.Deck;
import hearthstone.configs.CollectionStateConfigs;
import hearthstone.configs.ShopStateConfigs;
import hearthstone.ui.UICardImage;
import hearthstone.ui.UIObject;
import java.util.ArrayList;
public class CardInitUtils {
    private static ShopStateConfigs s = ShopStateConfigs.getInstance();
    private static CollectionStateConfigs c = CollectionStateConfigs.getInstance();

    public static void arrangeCards(ArrayList<UICardImage> uiCardImages) {
        int counter = 0;
        float xPosCard = s.get("initialXPosCard");
        float yPosCard = s.get("initialYPosCard");
        for (UICardImage uiCardImage : uiCardImages) {
            counter++;
            uiCardImage.setX(xPosCard);
            uiCardImage.setY(yPosCard);
            xPosCard += s.get("widthCard") + s.get("horizontalDistanceCard");
            if (counter == s.get("maxCardRow") || counter == s.get("maxCardRow")*2) {
                yPosCard += s.get("heightCard") + s.get("verticalDistanceCard");
                xPosCard -= (s.get("widthCard") + s.get("horizontalDistanceCard")) * s.get("maxCardRow");
            } else if (counter == s.get("maxCardRow") * s.get("maxCardColumn")) {
                xPosCard = s.get("initialXPosCard");
                yPosCard = s.get("initialYPosCard");
                counter = 0;
            }
        }
    }

//    public static ArrayList<UICardImage> amountFilter(int page, ArrayList<UICardImage> uiCardImages){
//        ArrayList<UICardImage> manager = new ArrayList<>();
//        if (uiCardImages.size() <= 6)
//            return uiCardImages;
//        for (int i = 2 ; i < 4 ; i++){
//            if (uiCardImages.size() <= i*6){
//                for (int j = 1 ; j <= i ; j++){
//                    if (page == j){
//                        for (int k = (j-1)*6 ; k < Math.min(j*6, uiCardImages.size()) ; k++) {
//                                manager.add(uiCardImages.get(k));
//                        }
//                        break;
//                    }
//                }
//                break;
//            }
//        }
//        return manager;
//    }

    public static ArrayList<UICardImage> amountFilter(int page, ArrayList<UICardImage> uiCardImages){
        ArrayList<UICardImage> manager = new ArrayList<>();
        if (uiCardImages.size() <= 24)
            return uiCardImages;
        else if (uiCardImages.size() <= 48){
            if (page == 1) {
                for (int i = 0; i < 24; i++) {
                    manager.add(uiCardImages.get(i));
                }
            }else if (page == 2){
                for (int i = 24; i < uiCardImages.size(); i++) {
                    manager.add(uiCardImages.get(i));
                }
            }
            return manager;
        }else return null;
    }

    public static boolean canSell(Deck[] decks, Card card){
        for (Deck deck : decks) {
            if (deck != null){
                for (Card deckCard : deck.getCards()) {
                    if (deckCard.equals(card))
                        return false;
                }
            }
        }
        return true;
    }
}

