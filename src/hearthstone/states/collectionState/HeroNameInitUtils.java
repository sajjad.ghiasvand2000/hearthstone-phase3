package hearthstone.states.collectionState;

import Entity.Hero;
import Entity.HeroClass;
import Entity.cards.Card;
import Entity.cards.Deck;
import hearthstone.ui.UIDoubleImage;
import hearthstone.ui.UIObject;

import java.util.ArrayList;

public class HeroNameInitUtils {

    public static HeroClass recognizeHero(int i){
        if (i == 0)
            return HeroClass.MAGE;
        else if(i == 1)
            return HeroClass.WARLOCK;
        else if (i == 2)
            return HeroClass.ROGUE;
        else if (i == 3)
            return HeroClass.PALADIN;
        else if (i == 4)
            return HeroClass.HUNTER;
        return null;
    }

    public static int recognizeHero(HeroClass heroClass){
        if (heroClass.equals(HeroClass.MAGE))
            return 1;
        else if(heroClass.equals(HeroClass.WARLOCK))
            return 2;
        else if (heroClass.equals(HeroClass.ROGUE))
            return 3;
        else if (heroClass.equals(HeroClass.PALADIN))
            return 4;
        else if (heroClass.equals(HeroClass.HUNTER))
            return 5;
        return 0;
    }
    public static ArrayList<UIObject> toUIObject(ArrayList<UIDoubleImage> uiDoubleImages){
        ArrayList<UIObject> uiObjects = new ArrayList<>(uiDoubleImages);
        return uiObjects;
    }

    public static boolean canChangeHero(Deck deck){
        for (Card card : deck.getCards()) {
            if (card.getHeroClass().equals(deck.getHero()))
                return false;
        }
        return true;
    }
}
