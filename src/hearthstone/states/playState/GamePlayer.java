package hearthstone.states.playState;

import Entity.cards.Card;
import Entity.cards.Deck;
import Entity.cards.Type;
import data.Log;
import hearthstone.Handler;
import hearthstone.Launcher;
import hearthstone.configs.PlayStateConfigs;
import hearthstone.constants.Constants;
import hearthstone.gfx.Asserts;
import hearthstone.gfx.Drawer;
import hearthstone.gfx.ImageLoader;
import hearthstone.states.collectionState.CardInitUtils;
import hearthstone.states.collectionState.CardNameInitUtils;
import hearthstone.states.collectionState.HeroNameInitUtils;
import hearthstone.ui.*;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;

public class GamePlayer {
    private PlayStateConfigs p = PlayStateConfigs.getInstance();
    private Handler handler;
    private Deck deck;
    private ArrayList<UICardImage> containLandUI;
    private ArrayList<UIDoubleImage> containDeckUI;
    private ArrayList<Card> containLandCard;
    private ArrayList<Card> containDeckCard;
    private UIManager uiManagerLand;
    private UIManager uiManagerDeck;
    private UIManager uiManagerHero;
    private ArrayList<String> event;
    private int level = 1;
    private int controllerCounter = 0;
    private boolean controller = false;
    private int offCardsPassive;
    private int ManaJumpPassive;
    private int mana;

    public GamePlayer(Handler handler, Deck deck) {
        this.handler = handler;
        this.deck = deck;
        containDeckUI = new ArrayList<>();
        containLandUI = new ArrayList<>();
        containLandCard = new ArrayList<>();
        containDeckCard = new ArrayList<>();
        uiManagerDeck = new UIManager(handler);
        uiManagerLand = new UIManager(handler);
        uiManagerHero = new UIManager(handler);
        event = new ArrayList<>();
        offCardsPassive = handler.getInfoPassive().getOffCardPassive();
        ManaJumpPassive = handler.getInfoPassive().getManaJumpPassive();
        mana = 1 + ManaJumpPassive;
        Collections.addAll(containDeckCard, deck.getCards().get(0), deck.getCards().get(1), deck.getCards().get(2));
        cardDeckInitFirst();
        cardLandInitFirst();
        cardLandInit();
        cardDeckInit();
        cardDeckInit();
        heroInit();
    }

    public void tick() {
        if (controller) {
            controllerCounter++;
            if (controllerCounter == 20) {
                controllerCounter = 0;
                controller = false;
            }
        }
        uiManagerDeck.tick();
        uiManagerLand.tick();
        uiManagerHero.tick();
    }

    public void render(Graphics2D g2D) {
        Drawer.mana("Mana: " + mana + "/" + level, g2D);
        uiManagerHero.render(g2D);
        uiManagerLand.render(g2D);
        uiManagerDeck.render(g2D);
        int height = p.get("initialYPosEvent");
        for (String s : event) {
            drawer(g2D, s, height);
            height += p.get("horizontalDistanceEvent");
        }
    }

    public void cardDeckInitFirst() {
        for (Card card : deck.getCards()) {
            containDeckUI.add(new UIDoubleImage(card, deck.getCards().size(), 0, 0, 0, 0, 0, p.get("widthCardBig"),
                    p.get("heightCardBig"), ImageLoader.loadDoubleImage(card.getTexturePath()[0], card.getTexturePath()[0]), () -> {
                if (controllerCounter == 0) {
                    if (mana >= card.getMana() - offCardsPassive) {
                        mana -= card.getMana() - offCardsPassive;
                        containDeckCard.remove(card);
                        if (containLandCard.size() < 7 && card.getType().equals(Type.MINION)) {
                            containLandCard.add(card);
                            cardLandInit();
                        }
                        cardDeckInit();
                        event.add(card.getName());
                        Log.body("click card in play state", "play " + card.getName());
                    }
                    controllerCounter++;
                    controller = true;
                }
            }));
        }
    }

    public void cardDeckInit() {
        ArrayList<UIDoubleImage> manager;
        manager = Utils.findUIDoubleImage(containDeckUI, containDeckCard);
        Utils.arrangeCardDeck(manager);
        uiManagerDeck.setButtons(CardNameInitUtils.toUIObject(manager));
    }

    public void cardLandInitFirst() {
        for (Card card : deck.getCards()) {
            containLandUI.add(new UICardImage(card, 0, 0, 0, 0, ImageLoader.loadDoubleImage(card.getTexturePath()[0], card.getTexturePath()[0]), () -> {

            }));
        }
    }

    public void cardLandInit() {
        ArrayList<UICardImage> manager;
        manager = Utils.findUICardImage(containLandUI, containLandCard);
        Utils.arrangeCardLand(manager);
        uiManagerLand.setButtons(CardInitUtils.toUIObject(manager));
    }

    public void heroInit() {
        uiManagerHero.addButton(new UIRecImage(p.get("initialXPosHero"), p.get("initialYPosHero"), p.get("widthHero"), p.get("heightHero"),
                Asserts.heroesPlay[HeroNameInitUtils.recognizeHero(deck.getHero().getHeroClass()) - 1], () -> {}
        ));

        uiManagerHero.addButton(new UIRecImage(p.get("initialXPosHeroPower"), p.get("initialYPosHeroPower"), p.get("widthHeroPower"), p.get("heightHeroPower"),
                Asserts.heroesHeroPowerPlay[HeroNameInitUtils.recognizeHero(deck.getHero().getHeroClass()) - 1], () -> {}
        ));
    }

    private void drawer(Graphics2D g2D, String prompt, int height) {
        Font font = new Font("Helvetica", Font.BOLD, 10);
        g2D.setColor(Color.BLUE);
        g2D.setFont(font);
        g2D.drawString(prompt, p.get("initialXPosEvent"), height);
    }

    public Handler getHandler() {
        return handler;
    }

    public Deck getDeck() {
        return deck;
    }

    public int getMana() {
        return mana;
    }

    public int getLevel() {
        return level;
    }


    public void setHandler(Handler handler) {
        this.handler = handler;
    }

    public void setDeck(Deck deck) {
        this.deck = deck;
    }

    public void setMana(int mana) {
        this.mana = mana;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public ArrayList<Card> getContainDeckCard() {
        return containDeckCard;
    }
}
