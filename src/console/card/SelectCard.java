//package console.card;
//
//import Entity.MainPlayer;
//import console.State;
//import data.Log;
//
//import java.io.IOException;
//import java.util.Scanner;
//
//public class SelectCard {
//
//    public static State steState() throws IOException {
//        MainPlayer m = MainPlayer.getInstance();
//        Scanner scanner = new Scanner(System.in);
//        System.out.println("Choose one of these:");
//        System.out.println("ls-a-cards\nls-m-cards\nls-n-cards\nadd[card's name]\nremove[card's name]\nhearthstone--help\nback");
//        String selectCard = scanner.nextLine();
//        switch (selectCard) {
//            case "ls-a-cards":
//                Log.body(m.getUserId(), m.getUsername(), "select", "card:" + m.getHeroes().get(m.getIndex()).getHeroClass());
//                return State.ls_a_cards;
//            case "ls-m-cards":
//                Log.body(m.getUserId(), m.getUsername(), "select", "card:deck");
//                return State.ls_m_cards;
//            case "ls-n-cards":
//                Log.body(m.getUserId(), m.getUsername(), "select", "card:not in deck");
//                return State.ls_n_cards;
//            case "back":
//                Log.body(m.getUserId(), m.getUsername(), "back", "return to collections");
//                return State.collections;
//            case "hearthstone--help":
//                System.out.println("ls-a-cards:all cards of your hero.");
//                System.out.println("ls-m-cards:all cards of your deck.");
//                System.out.println("ls-n-cards:all cards which aren't in your deck but you can add them to your deck.");
//                System.out.println("add[card's name]:You can add a card to your deck.");
//                System.out.println("remove[card's name]:You can remove a card from your deck.");
//                System.out.println("hearthstone--help\nback");
//                System.out.println("------------------------------");
//                Log.body(m.getUserId(), m.getUsername(), "help", "hearthstone--help");
//                return State.selectCard;
//            default:
////                int counter = 0;
////                if (selectCard.length() >= 6 && selectCard.substring(0, 3).equals("add")) {
////                    String s = selectCard.substring(4, selectCard.lastIndexOf(']'));
////                    for (int i = 0; i < m.getCurrentCards().size(); i++) {
////                        if (s.equals(m.getCurrentCards().get(i).getName())) {
////                            counter++;
////                        }
////                    }
////                    if (MyFile.doesExistInFile(s, "cards.txt")) {
////                        if (MyFile.doesExistInEntireCards(s)) {
////                            if (m.getCurrentCards().size() < 15 && counter <= 1) {
////                                if (m.addRemoveCurrentCard(s, "add")) {
////                                    MyFile.deletePlayer("player.txt", m.getPassword());
////                                    MyFile.fileWriter("player.txt", m);
////                                    Log.body(m.getUserId(), m.getUsername(), "add", "add a card to the deck: " + s);
////                                    System.out.println("This card added to your deck.");
////                                }
////                            } else if (m.getCurrentCards().size() >= 15) {
////                                System.out.println("Your deck is full!");
////                                Log.body(m.getUserId(), m.getUsername(), "add", "deck is full");
////                            } else if (counter > 1) {
////                                System.out.println("Two cards with this name have already existed.");
////                                Log.body(m.getUserId(), m.getUsername(), "add", "error:add repetitive card.");
////                            }
////                        } else {
////                            System.out.println("You don't have this card.");
////                            Log.body(m.getUserId(), m.getUsername(), "add", "error:don't have");
////                        }
////                    } else {
////                        System.out.println("There is not any card with this name.");
////                        Log.body(m.getUserId(), m.getUsername(), "add", "error:doesn't exist");
////                    }
////                } else if (selectCard.length() >= 9 && selectCard.substring(0, 6).equals("remove")) {
////                    String s1 = selectCard.substring(7, selectCard.lastIndexOf(']'));
////                    if (m.getCurrentCards().size() > 0) {
////                        if (MyFile.doesExistInFile(s1, "cards.txt")) {
////                            if (MyFile.doesExistInCurrentCards(s1)) {
////                                if (m.getCurrentCards().size() > 0)
////                                    if (m.addRemoveCurrentCard(s1, "remove")) {
////                                        MyFile.deletePlayer("player.txt", m.getPassword());
////                                        MyFile.fileWriter("player.txt", m);
////                                        Log.body(m.getUserId(), m.getUsername(), "remove", "remove a card from deck: " + s1);
////                                        System.out.println("This card deleted.");
////                                    }
////
////
////                            } else {
////                                System.out.println("This card is not in your deck.");
////                                Log.body(m.getUserId(), m.getUsername(), "add", "error:don't have");
////                            }
////                        } else {
////                            System.out.println("There is not any card with this name.");
////                            Log.body(m.getUserId(), m.getUsername(), "add", "error:doesn't exist");
////                        }
////                    } else {
////                        System.out.println("Your deck is empty!");
////                        Log.body(m.getUserId(), m.getUsername(), "remove", "deck is empty");
////                    }
////                } else {
////                    System.out.println("Inputs are incorrect.");
////                    Log.body(m.getUserId(), m.getUsername(), "error", "incorrect inputs");
////                }
//                return State.selectCard;
//        }
//    }
//}