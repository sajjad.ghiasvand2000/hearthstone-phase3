package data;

import Entity.HeroClass;
import Entity.cards.Card;
import Entity.Hero;
import Entity.cards.Deck;
import com.google.gson.Gson;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Setter {
    public static List<Hero> setHero() throws FileNotFoundException {
        List<Hero> heroes = new ArrayList<>();
        Scanner fileReader = new Scanner(new FileReader("Heroes.txt"));
        while (fileReader.hasNext()){
            Hero hero = new Gson().fromJson(fileReader.nextLine(), Hero.class);
            if (hero.getHeroClass().toString().equals("MAGE"))
                hero.setLock(false);
            heroes.add(hero);
        }
        fileReader.close();
        return heroes;
    }

    public static ArrayList<Card> setCard() throws FileNotFoundException {
        ArrayList<Card> cards = new ArrayList<>();
        Scanner fileReader = new Scanner(new FileReader("cards.txt"));
        for (int i = 0; i < 37; i++) {
            Card card = new Gson().fromJson(fileReader.nextLine(), Card.class);
            cards.add(card);
            if (i < 10)
                card.setLock(false);
        }
        return cards;
    }
//
//    public static Deck setDeck() throws FileNotFoundException {
//        List<Card> cards = new ArrayList<>();
//        Scanner fileReader = new Scanner(new FileReader("cards.txt"));
//        for (int i = 0; i < 3; i++) {
//            Card card = new Gson().fromJson(fileReader.nextLine(), Card.class);
//            cards.add(card);
//            cards.add(card);
//        }
//        for (int i = 3; i < 7; i++) {
//            Card card = new Gson().fromJson(fileReader.nextLine(), Card.class);
//            cards.add(card);
//
//        }
//        return new Deck(cards, Setter.setHero().get(0));
//    }
}
