package hearthstone.configs;

public class CollectionStateConfigs {
    private String name;
    private Configs properties;
    private static CollectionStateConfigs collectionStateConfigs;

    public CollectionStateConfigs(String name){
        properties = ConfigLoader.getInstance().getCollectionStateConfigs(name);
    }

    public static CollectionStateConfigs getInstance(String name){
        if (collectionStateConfigs == null)
            collectionStateConfigs = new CollectionStateConfigs(name);
        return collectionStateConfigs;
    }

    public static CollectionStateConfigs getInstance(){
        return getInstance("COLLECTION_STATE_CONFIG_FILE");
    }

    public int get(String name){
        return properties.readInteger(name);
    }
}
