package console;

public enum State {
    preLogin,
    login,
    signUp,
    enter,
    deletePlayer,
    collections,
    store,
    ls_a_heroes,
    ls_m_heroes,
    ls_a_cards,
    ls_m_cards,
    ls_n_cards,
    selectCard,
    ls_s,
    ls_b

}
