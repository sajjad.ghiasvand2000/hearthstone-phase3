package hearthstone.login;

import Entity.Hero;
import Entity.cards.Card;
import Entity.MainPlayer;
import Entity.cards.Deck;
import data.Log;
import data.MyFile;
import data.Setter;
import hearthstone.Game;
import hearthstone.Launcher;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class RegisterState {
    private String username;
    private String password;


    public RegisterState(String username, String password) throws IOException {
        this.username = username;
        this.password = password;
        init();
    }

    private void init() throws IOException {
        if (MyFile.isRepetitiveUsername(username, "player.txt")) {
            LoginFrame.getTextArea().append("This username has already existed please choose another one!\n");
            return;
        }
        int numberId = Log.userId();
        List<Hero> heroes = Setter.setHero();
        ArrayList<Card> cards = Setter.setCard();
        MainPlayer.getInstance().setUserId(numberId);
        MainPlayer.getInstance().setUsername(username);
        MainPlayer.getInstance().setPassword(password);
        MainPlayer.getInstance().setHeroes(heroes);
        MainPlayer.getInstance().setEntireCards(cards);
        MainPlayer.getInstance().setDiamond(50);
        MainPlayer.getInstance().setDecks(new Deck[13]);
        MyFile.fileWriter("player.txt", MainPlayer.getInstance());
        Log.createLog(numberId, username, password);
        LoginFrame.getTextArea().append("Congratulation!You created an account successfully.\n");
        Launcher.setGame(new Game(1700, 1000, "Hearthstone"));
        Launcher.getGame().start();
    }
}
