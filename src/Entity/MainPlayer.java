package Entity;

public class MainPlayer extends Player {
    private int index;
    private static MainPlayer instance = new MainPlayer();
    private MainPlayer(){}
    public static MainPlayer getInstance(){
        return instance;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getIndex() {
        return index;
    }
}
