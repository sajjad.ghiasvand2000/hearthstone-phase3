//package console.store;
//
//import Entity.cards.Card;
//import Entity.Hero;
//import com.google.gson.Gson;
//import Entity.MainPlayer;
//import console.State;
//import data.Log;
//import data.MyFile;
//
//import java.io.FileReader;
//import java.io.IOException;
//import java.util.Scanner;
//
//public class Store {
//
//    public static State setState() throws IOException {
//        MainPlayer m = MainPlayer.getInstance();
//        Scanner scanner = new Scanner(System.in);
//        System.out.println("Choose one of these:\nbuy[card's name]\nwallet\nsell[card's name]\nls-s\nls-b\nhearthstone--help\nback");
//        String next = scanner.nextLine();
//        switch (next) {
//            case "wallet":
//                Log.body(m.getUserId(), m.getUsername(), "store", "you're wallet");
//                System.out.println("Your wallet:" + m.getDiamond());
//                return State.store;
//            case "ls-s":
//                Log.body(m.getUserId(), m.getUsername(), "store", "sellable cards");
//                return State.ls_s;
//            case "ls-b":
//                Log.body(m.getUserId(), m.getUsername(), "store", "buyable cards");
//                return State.ls_b;
//            case "hearthstone--help":
//                System.out.println("buy[card's name]:If you want to buy card.\nwallet:It will show your wallet.");
//                System.out.println("sell[card's name]:If you want to sell card.");
//                System.out.println("ls-s:All cards that you can sell them.\nls-b:All cards that you can buy.");
//                System.out.println("hearthstone--help\nback");
//                System.out.println("---------------------------------");
//                Log.body(m.getUserId(), m.getUsername(), "help", "hearthstone--help");
//                return State.store;
//            case "back":
//                Log.body(m.getUserId(), m.getUsername(), "back", "return to main menu");
//                return State.enter;
//            default:
//                boolean flag = false;
//                boolean flag2 = false;
//                if (next.length() >= 6 && next.substring(0, 3).equals("buy")) {
//                    String s = next.substring(4, next.lastIndexOf(']'));
//                    if (!MyFile.doesExistInEntireCards(s)) {
//                        Scanner fileReader = new Scanner(new FileReader("cards.txt"));
//                        while (fileReader.hasNext()) {
//                            Card card = new Gson().fromJson(fileReader.nextLine(), Card.class);
//                            if (card.getName().equals(s)) {
//                                flag = true;
//                                if (m.getDiamond() >= card.getCost()) {
//                                    m.setDiamond(m.getDiamond() - card.getCost());
//                                    m.addRemoveEntireCard(s, "add");
//                                    MyFile.deletePlayer("player.txt", m.getPassword());
//                                    MyFile.fileWriter("player.txt", m);
//                                    System.out.println("Buying was successfully.");
//                                    Log.body(m.getUserId(), m.getUsername(), "buy", "buy:" + s);
//
//                                } else {
//                                    System.out.println("Not enough money!");
//                                    Log.body(m.getUserId(), m.getUsername(), "buy", "not enough money");
//                                }
//                                fileReader.close();
//                                break;
//                            }
//                        }
//                    } else {
//                        System.out.println("You have this card!");
//                        Log.body(m.getUserId(), m.getUsername(), "buy", "this card existed");
//                        flag2 = true;
//                    }
//                    if (!flag && !flag2) {
//                        System.out.println("There is not any card with this name.");
//                        Log.body(m.getUserId(), m.getUsername(), "buy", "error:doesn't exist");
//                    }
//
//                } else if (next.length() >= 7 && next.substring(0, 4).equals("sell")) {
//                    String s1 = next.substring(5, next.lastIndexOf(']'));
//                    if (MyFile.doesExistInEntireCards(s1)) {
//                        boolean flag1 = false;
//                        for (Hero hero : MainPlayer.getInstance().getHeroes()) {
//                            for (Card heroCard : hero.getCards()) {
//                                if (heroCard.getName().equals(s1)) {
//                                    flag1 = true;
//                                    break;
//                                }
//                            }
//                            if (flag1) break;
//                        }
//                        if (flag1) {
//                            System.out.println("You can't sell this card.");
//                            Log.body(m.getUserId(), m.getUsername(), "sell", "error:this card exists in the deck");
//                        } else {
//                            Scanner fileReader = new Scanner(new FileReader("cards.txt"));
//                            while (fileReader.hasNext()) {
//                                Card card = new Gson().fromJson(fileReader.nextLine(), Card.class);
//                                if (card.getName().equals(s1)) {
//                                    m.setDiamond(m.getDiamond() + card.getCost());
//                                    m.addRemoveEntireCard(s1, "remove");
//                                    MyFile.deletePlayer("player.txt", m.getPassword());
//                                    MyFile.fileWriter("player.txt", m);
//                                    System.out.println("Selling was successfully.");
//                                    Log.body(m.getUserId(), m.getUsername(), "sell", "sell:" + s1);
//                                    fileReader.close();
//                                    break;
//                                }
//                            }
//                        }
//                    } else {
//                        if (MyFile.doesExistInFile(s1, "cards.txt")) {
//                            System.out.println("You don't have this card!");
//                            Log.body(m.getUserId(), m.getUsername(), "sell", "don't have");
//                        }else {
//                            System.out.println("There is not any card with this name.");
//                            Log.body(m.getUserId(), m.getUsername(), "sell", "error:doesn't exist");
//                        }
//                    }
//                } else {
//                    System.out.println("Inputs are incorrect.");
//                    Log.body(m.getUserId(), m.getUsername(), "error", "incorrect inputs");
//                }
//        }
//        return State.store;
//    }
//}
//
