package hearthstone.input;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class KeyManager implements KeyListener {
    private boolean[] keys;
    public KeyManager(){
        keys = new boolean[256];
    }

    public void tick(){

    }
    @Override
    public void keyTyped(KeyEvent keyEvent) {

    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {
        if (keyEvent.getKeyCode() < 256) {
            keys[keyEvent.getKeyCode()] = true;
        }

    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {
        if (keyEvent.getKeyCode() < 256) {
            keys[keyEvent.getKeyCode()] = false;
        }
    }

    public boolean[] getKeys() {
        return keys;
    }
}
