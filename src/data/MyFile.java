package data;

import Entity.cards.Card;
import Entity.Player;
import com.google.gson.Gson;
import Entity.MainPlayer;

import java.io.*;
import java.util.Scanner;

public class MyFile {

    public static void fileWriter(String fileName, Object object) throws IOException {
        String data = new Gson().toJson(object);
        FileWriter fileWriter = new FileWriter(fileName, true);
        fileWriter.write(data + "\r\n");
        fileWriter.flush();
        fileWriter.close();
    }

    public static void deletePlayer(String fileName, String password) throws IOException {
        FileWriter fileWriter = new FileWriter("player1.txt");
        Scanner Reader = new Scanner(new FileReader(fileName));

        while (Reader.hasNext()) {
            String data = Reader.nextLine();
            Player player2 = new Gson().fromJson(data, Player.class);
            if (password.equals(player2.getPassword())) continue;
            else {
                fileWriter.write(data + "\r\n");
            }
            fileWriter.flush();
        }
        Reader.close();
        fileWriter.close();
        File file1 = new File("player1.txt");
        File file = new File(fileName);
        file.delete();
        file1.renameTo(file);
    }

    public static boolean isRepetitiveUsername(String username, String fileName) throws FileNotFoundException {
        Scanner fileReader = new Scanner(new FileReader(fileName));
        while (fileReader.hasNext()) {
            Player player = new Gson().fromJson(fileReader.nextLine(), Player.class);
            if (player.getUsername().equals(username)) {
                fileReader.close();
                return true;
            }
        }
        fileReader.close();
        return false;
    }

    public static void saveChanges() {
        try {
            MyFile.deletePlayer("player.txt", MainPlayer.getInstance().getPassword());
            MyFile.fileWriter("player.txt", MainPlayer.getInstance());
        }catch (IOException e){
            e.getStackTrace();
        }
    }

//    public static boolean doesExistInFile(String name ,String fileName) throws FileNotFoundException {
//        Scanner fileReader = new Scanner(new FileReader(fileName));
//        while (fileReader.hasNext()){
//            Card card = new Gson().fromJson(fileReader.nextLine() , Card.class);
//            if (card.getName().equals(name)){
//                fileReader.close();
//                return true;
//            }
//        }
//        fileReader.close();
//        return false;
//    }
//
//    public static boolean doesExistInEntireCards(String cardName) {
//        for (Card card : MainPlayer.getInstance().getEntireCards()) {
//            if (card.getName().equals(cardName)) {
//                return true;
//            }
//        }
//        return false;
//    }

//    public static boolean doesExistInCurrentCards(String cardName) {
//        for (Card card : MainPlayer.getInstance().getCurrentCards()) {
//            if (card.getName().equals(cardName)) {
//                return true;
//            }
//        }
//        return false;
//    }
}
