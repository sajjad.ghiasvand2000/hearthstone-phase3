package hearthstone.constants;

public class Constants {
    public static final int COMPUTER_WIDTH = 1550;
    public static final int COMPUTER_HEIGHT = 878;
    public static final float HEIGHT_DIVIDED_BY_WIDTH = 1.38F;

}
