package hearthstone.states.shopState;

import Entity.cards.Card;
import Entity.MainPlayer;
import data.Log;
import hearthstone.Handler;
import hearthstone.configs.ShopStateConfigs;
import hearthstone.gfx.Asserts;
import hearthstone.gfx.Drawer;
import hearthstone.gfx.ImageLoader;
import hearthstone.states.Errors;
import hearthstone.states.State;
import hearthstone.states.collectionState.CardInitUtils;
import hearthstone.ui.UICardImage;
import hearthstone.ui.UIManager;
import hearthstone.utils.Util;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ShopState extends State {
    private ShopStateConfigs s = ShopStateConfigs.getInstance();
    private UIManager uiManager;
    private ArrayList<UICardImage> containCards;
    private ConstantButton constantButton;
    private boolean showUnLockCards = false;
    private boolean showLockCards = true;
    private int page = 1;
    private static String errorMassage = null;
    private static boolean error = false;

    public ShopState(Handler handler) throws IOException {
        super(handler);
        uiManager = new UIManager(handler);
        containCards = new ArrayList<>();
        constantButton = new ConstantButton(handler);
        cardInitFirst();
        setPicture();
        cardInit();
    }

    @Override
    public void tick() {
        uiManager.tick();
        constantButton.tick();
    }

    @Override
    public void render(Graphics2D g2D) {
        g2D.drawImage(Asserts.shopBackground, 0, 0, 1550, 878, null);
        uiManager.render(g2D);
        constantButton.render(g2D);
        if (errorMassage != null) {
            Errors.ErrorMassageShop(g2D, errorMassage);
        }
        Drawer.Wallet("Wallet: " + MainPlayer.getInstance().getDiamond(), g2D);
    }

    public void updateSetPicture() {
        try {
            setPicture();
        } catch (IOException e) {
            e.getStackTrace();
        }
    }

    public void cardInitFirst() {
        for (Card card : MainPlayer.getInstance().getEntireCards()) {
            containCards.add(new UICardImage(card, 0, 0, s.get("widthCard"), s.get("heightCard"),
                    null, () -> {
                if (card.isLock()) {
                    if (MainPlayer.getInstance().getDiamond() >= card.getCost()) {
                        Log.body("click card in shop", "buy " + card.getName());
                        MainPlayer.getInstance().getEntireCards().get(Util.searchMainPlayerCards(card)).setLock(false);
                        MainPlayer.getInstance().setDiamond(MainPlayer.getInstance().getDiamond() - card.getCost());
                        containCards.get(Util.searchMainPlayerCards(card)).getCard().setLock(false);
                        updateSetPicture();
                        cardInit();
                    } else {
                        error = true;
                        errorMassage = "Not enough money!";
                        Log.body("error in shop state", "Not enough money!");
                    }
                } else {
                    if (hearthstone.states.shopState.CardInitUtils.canSell(MainPlayer.getInstance().getDecks(), card)) {
                        Log.body("click card in shop state", "sell " + card.getName());
                        MainPlayer.getInstance().getEntireCards().get(Util.searchMainPlayerCards(card)).setLock(true);
                        MainPlayer.getInstance().setDiamond(MainPlayer.getInstance().getDiamond() + card.getCost());
                        containCards.get(Util.searchMainPlayerCards(card)).getCard().setLock(true);
                        updateSetPicture();
                        cardInit();
                    } else {
                        error = true;
                        errorMassage = "This card exists in a deck!";
                        Log.body("error in shop state", "This card exists in a deck!");
                    }
                }
            }));
        }
    }

    public void cardInit() {
        ArrayList<UICardImage> manager;
        manager = CardInitUtils.lockFilter(showUnLockCards, showLockCards, containCards);
        hearthstone.states.shopState.CardInitUtils.arrangeCards(manager);
        manager = hearthstone.states.shopState.CardInitUtils.amountFilter(page, manager);
        uiManager.setButtons(CardInitUtils.toUIObject(manager));
    }

    public void setPicture() throws IOException {
        for (UICardImage uiCardImage : containCards) {
            if (uiCardImage.getCard().isLock())
                uiCardImage.setImages(ImageLoader.loadDoubleImage(uiCardImage.getCard().getTexturePath()[7], uiCardImage.getCard().getTexturePath()[10]));
            else
                uiCardImage.setImages(ImageLoader.loadDoubleImage(uiCardImage.getCard().getTexturePath()[0], uiCardImage.getCard().getTexturePath()[10]));
        }
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public void setShowUnLockCards(boolean showUnLockCards) {
        this.showUnLockCards = showUnLockCards;
    }

    public void setShowLockCards(boolean showLockCards) {
        this.showLockCards = showLockCards;
    }

    public static boolean isError() {
        return error;
    }

    public static void setErrorMassage(String errorMassage) {
        ShopState.errorMassage = errorMassage;
    }

    public static void setError(boolean error) {
        ShopState.error = error;
    }
}
