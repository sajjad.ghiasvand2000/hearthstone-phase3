package hearthstone.states.collectionState;

import Entity.HeroClass;
import Entity.cards.Card;
import Entity.cards.Deck;
import Entity.MainPlayer;
import data.Log;
import hearthstone.Handler;
import hearthstone.configs.CollectionStateConfigs;
import hearthstone.gfx.Asserts;
import hearthstone.gfx.Drawer;
import hearthstone.gfx.ImageLoader;
import hearthstone.gfx.Input;
import hearthstone.states.Errors;
import hearthstone.states.State;
import hearthstone.states.infoPassive.Utils;
import hearthstone.ui.*;
import hearthstone.ui.UIManager;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.*;
import java.util.List;

public class CollectionState extends State {
    private CollectionStateConfigs c = CollectionStateConfigs.getInstance();
    private ConstantButton constantButton;
    private ArrayList<UICardImage> containCards;
    private List<UIDoubleImage> containNameCards;
    private ArrayList<UIDoubleImage> containHeroName;
    private ArrayList<UIObject> containDeckButton;
    private ArrayList<UIRecImage> containWriteHeroName;
    private int mana = -1;
    private HeroClass heroClass = HeroClass.NATURAL;
    private int deckIndex;
    private int page = 1;
    private boolean showUnLockCards = false;
    private boolean showLockCards = false;
    private Deck currentDeck = null;
    private Deck playDeck;
    private UIManager uiManager2;
    private UIManager uiManager3;
    private static String errorMassage = null;
    private static boolean error = false;
    private Input searchInput;

    public CollectionState(Handler handler) throws IOException {
        super(handler);
        if (MainPlayer.getInstance().getDecks()[0] != null)
            playDeck = MainPlayer.getInstance().getDecks()[0];
        uiManager2 = new UIManager(handler);
        uiManager3 = new UIManager(handler);
        containCards = new ArrayList<>();
        containNameCards = new ArrayList<>();
        containDeckButton = new ArrayList<>();
        containHeroName = new ArrayList<>();
        containWriteHeroName = new ArrayList<>();
        constantButton = new ConstantButton(handler);
        searchInput = new Input(handler, 0, 0, 0, 20);
        cardInitFirst();
        cardInit();
        deckButtonInitFirst();
        cardNameInitFirst();
        cardNameInit();
        deckButtonInit();
        heroNameButtonInitFirst();
        setDeckPictureInit();
        writeNameInitFirst1();
        writeNameInitFirst2();
    }

    @Override
    public void tick() {
        uiManager3.tick();
        uiManager2.tick();
        constantButton.tick();
        searchInput.tick();
    }

    @Override
    public void render(Graphics2D g2D) {
        g2D.drawImage(Asserts.collectionBackground, 0, 0, 1550, 878, null);
        Drawer.rightBorderCollection(g2D);
        constantButton.render(g2D);
        uiManager3.render(g2D);
        uiManager2.render(g2D);
        if (errorMassage != null) {
            Errors.ErrorMassageCollection(g2D, errorMassage);
        }
        searchInput.render(g2D);
    }

    public void updateCardInit() {
        try {
            cardInit();
        } catch (IOException e) {
            e.getStackTrace();
        }
    }

    public void updateCardNameInit() {
        try {
            cardNameInit();
        } catch (IOException e) {
            e.getStackTrace();
        }
    }

    public void heroNameButtonInitFirst() {
        for (int i = 0; i < c.get("heroNumber"); i++) {
            float y = c.get("initialYPosHeroNameButton") + (i + 1) * c.get("verticalDistanceHeroNameButton") + i * c.get("heightHeroNameButton");
            int finalI = i;
            containHeroName.add(new UIDoubleImage(containCards.get(0).getCard(), c.get("initialXPosHeroNameButton") + c.get("horizontalDistanceHeroNameButton"), y,
                    c.get("initialXPosHeroNameButton") + c.get("horizontalDistanceHeroNameButton") - c.get("widthCard"),
                    (float) (y - c.get("heightCardName") / 2.0), c.get("widthHeroNameButton"), c.get("heightHeroNameButton"),
                    c.get("widthCard"), c.get("heightCard"), Asserts.heroName[i]
                    , () -> {
                if (currentDeck.isNullHeroClass()) {
                    Log.body("click hero name", "choosing hero for " + currentDeck.getName());
                    currentDeck.getHero().setHeroClass(HeroNameInitUtils.recognizeHero(finalI));
                } else {
                    if (HeroNameInitUtils.canChangeHero(currentDeck)) {
                        Log.body("click hero name ", "changing hero for " + currentDeck.getName());
                        currentDeck.setHero(MainPlayer.getInstance().getHeroes().get(finalI));
                    } else {
                        error = true;
                        errorMassage = "You can not change your hero!";
                        Log.body("error in collection state: click hero name", "You can not change your hero!");
                    }
                }
                writeNameInit();
            }));
        }
    }

    public void writeNameInitFirst1() {
        containWriteHeroName.add(new UIRecImage(c.get("initialXPosDeckButton") + c.get("horizontalDistanceDeckButton"),
                c.get("initialYPosDeckButton") + c.get("verticalDistanceDeckButton"),
                c.get("widthDoneWriteHeroName"), c.get("heightDoneWriteHeroName"), Asserts.writeName, () -> {
            BufferedImage[] bufferedImages = new BufferedImage[]{Asserts.writeName[1], Asserts.writeName[1]};
            containWriteHeroName.get(0).setImages(bufferedImages);
            searchInput = new Input(handler, 15, c.get("initialXPosWriteHeroName"), c.get("initialYPosWriteHeroName"), 20);

        }));
    }

    public void writeNameInitFirst2() {
        containWriteHeroName.add(new UIRecImage(c.get("initialXPosDeckButton") + c.get("horizontalDistanceDeckButton"),
                c.get("initialYPosDeckButton") + c.get("heightDoneWriteHeroName") + 2 * c.get("verticalDistanceDeckButton"),
                c.get("widthDoneWriteHeroName"), c.get("heightDoneWriteHeroName"), Asserts.doneWriteHeroName, () -> {
            if (!searchInput.getInput().equals("")) {
                Log.body("writing", "write a name for current deck");
                currentDeck.setName(searchInput.getInput());
                ImageLoader.writeOnImage(Asserts.heroPicture[HeroNameInitUtils.recognizeHero(currentDeck.getHero().getHeroClass()) - 1][0], searchInput.getInput());
                currentDeck.setTexturePath("/texture/new hero picture/" + searchInput.getInput() + MainPlayer.getInstance().getUserId() + ".png");
                containWriteHeroName.get(0).setImages(Asserts.writeName);
                searchInput = new Input(handler, 0, 0, 0, 20);
                updateCardNameInit();
            } else {
                error = true;
                errorMassage = "Write a name for your deck!";
                Log.body("error in collection state: done click", "Write a name for your deck!");
            }
        }));
    }

    public void writeNameInit() {
        uiManager2.setButtons(Utils.toUIObject(containWriteHeroName));
    }

    public void heroNameButtonInit() {
        uiManager2.setButtons(HeroNameInitUtils.toUIObject(containHeroName));
    }

    public void setDeckPictureInit() {
        int x = 0;
        for (Deck deck : MainPlayer.getInstance().getDecks()) {
            if (deck != null) {
                containDeckButton.get(x).setImages(ImageLoader.loadDoubleImage(deck.getTexturePath(), deck.getTexturePath()));

            } else containDeckButton.get(x).setImages(Asserts.lockButton);
            x++;
        }
    }

    public void setDeckPicture() {
        containDeckButton.get(deckIndex).setImages(ImageLoader.loadDoubleImage(currentDeck.getTexturePath(), currentDeck.getTexturePath()));
    }

    public void deckButtonInit() {
        ArrayList<UIObject> manager = new ArrayList<>();
        for (UIObject uiObject : containDeckButton) {
            manager.add(uiObject);
        }
        uiManager2.setButtons(manager);
    }

    private void deckButtonInitFirst() {
        for (int i = 0; i < c.get("deckNumber"); i++) {
            int finalI = i;
            containDeckButton.add(new UIRecImage(c.get("initialXPosDeckButton") + c.get("horizontalDistanceDeckButton"),
                    c.get("initialYPosDeckButton") + i * c.get("heightDeckButton") + (i + 1) * c.get("verticalDistanceDeckButton"),
                    c.get("widthDeckButton"), c.get("heightDeckButton"), Asserts.lockButton, () -> {
                if (MainPlayer.getInstance().getDecks()[finalI] != null) {
                    currentDeck = MainPlayer.getInstance().getDecks()[finalI];
                    Log.body("click deck", "choosing " + currentDeck.getName());
                    deckIndex = finalI;
                    updateCardInit();
                    updateCardNameInit();
                } else {
                    error = true;
                    errorMassage = "This deck is lock!";
                    Log.body("error in collection state: click deck", "This deck is lock!");
                }
            }));
        }
    }

    public void cardNameInit() throws IOException {
        if (currentDeck != null) {
            Collections.sort(containNameCards);
            ArrayList<UIDoubleImage> manager = new ArrayList<>();
            for (UIDoubleImage uiDoubleImage : containNameCards) {
                Card card = uiDoubleImage.getCard();
                int frequency = Collections.frequency(currentDeck.getCards(), card);
                if (frequency == 0) continue;
                CardNameInitUtils.setTexture(uiDoubleImage, frequency);
                manager.add(uiDoubleImage);
            }
            CardNameInitUtils.arrangeCardName(manager);
            uiManager2.setButtons(CardNameInitUtils.toUIObject(manager));
        }
    }

    private void cardNameInitFirst() {
        for (Card card : MainPlayer.getInstance().getEntireCards()) {
            containNameCards.add(new UIDoubleImage(card, c.get("initialXPosCardName") + c.get("horizontalDistanceCardName")
                    , 0, c.get("initialXPosCardName") + c.get("horizontalDistanceCardName") - c.get("widthCard"),
                    0, c.get("widthCardName"), c.get("heightCardName"), c.get("widthCard"), c.get("heightCard"), null
                    , () -> {
                if (Collections.frequency(currentDeck.getCards(), card) != 0) {
                    Log.body("click card name", "remove " + card.getName() + " from " + currentDeck.getName());
                    currentDeck.getCards().remove(card);
                    updateCardInit();
                    updateCardNameInit();
                }
            }));
        }
    }

    private void cardInitFirst() {
        for (Card card : MainPlayer.getInstance().getEntireCards()) {
            containCards.add(new UICardImage(card, 0, 0, c.get("widthCard"), c.get("heightCard"), null, () -> {
                if (!card.isLock()) {
                    if (currentDeck == null) {
//                        error = true;
//                        errorMassage = "You have not choose any decks!";
                    } else {
                        if (currentDeck.isNullHeroClass()) {
                            error = true;
                            errorMassage = "You have not choose any heroes!";
                            Log.body("error in collection state: click card", "You have not choose any heroes!");
                        } else {
                            if ((Collections.frequency(currentDeck.getCards(), card) != 2 && currentDeck.getCards().size() < 15 || currentDeck.isCardsNull())
                                    && (currentDeck.getHero().getHeroClass().equals(card.getHeroClass()) || card.getHeroClass().equals(HeroClass.NATURAL))) {
                                Log.body("click card", "add " + card.getName() + " to " + currentDeck.getName());
                                currentDeck.addCard(card);
                                updateCardInit();
                                updateCardNameInit();
                            } else if (currentDeck.getCards().size() >= 15) {
                                error = true;
                                errorMassage = "Your deck is full!";
                                Log.body("error in collection state: click card", "Your deck is full!");
                            } else if ((Collections.frequency(currentDeck.getCards(), card) == 2)) {
                                error = true;
                                errorMassage = "You have two cards from this type in your deck!";
                                Log.body("error in collection state: click card", "You have two cards from this type in your deck!");
                            } else if (!currentDeck.getHero().getHeroClass().equals(card.getHeroClass())) {
                                error = true;
                                errorMassage = "You can not choose card from this hero!";
                                Log.body("error in collection state: click card", "You can not choose card from this hero!");
                            }
                        }
                    }
                } else {
                    Log.body("click card", "go to shop state");
                    handler.getShopState().setShowLockCards(true);
                    handler.getShopState().setShowUnLockCards(false);
                    State.setCurrentState(handler.getShopState());
                }
            }));
        }
    }


    public void cardInit() throws IOException {
        ArrayList<UICardImage> manager;
        int frequency;
        manager = CardInitUtils.heroFilter(heroClass, containCards);
        manager = CardInitUtils.manaFilter(mana, manager);
        manager = CardInitUtils.lockFilter(showUnLockCards, showLockCards, manager);
        manager = CardInitUtils.searchFilter(constantButton.getSearchInput().getInput(), manager);
        for (UICardImage uiCardImage : manager) {
            Card card = uiCardImage.getCard();
            if (currentDeck == null) frequency = 0;
            else frequency = Collections.frequency(currentDeck.getCards(), card);
            if (card.isLock()) {
                uiCardImage.setImages(ImageLoader.loadDoubleImage(card.getTexturePath()[7], card.getTexturePath()[7]));
            } else {
                CardInitUtils.setTexture(uiCardImage, frequency);
            }
        }
        CardInitUtils.arrangeCards(manager);
        manager = CardInitUtils.amountFilter(page, manager);

        uiManager3.setButtons(CardInitUtils.toUIObject(manager));
    }


    //SETTERS AND GETTERS

    public void setHeroClass(HeroClass heroClass) {
        this.heroClass = heroClass;
    }

    public void setMana(int mana) {
        this.mana = mana;
    }

    public void setShowUnLockCards(boolean showUnLockCards) {
        this.showUnLockCards = showUnLockCards;
    }

    public void setShowLockCards(boolean showLockCards) {
        this.showLockCards = showLockCards;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPage() {
        return page;
    }

    public Deck getCurrentDeck() {
        return currentDeck;
    }

    public void setCurrentDeck(Deck currentDeck) {
        this.currentDeck = currentDeck;
    }

    public int getMana() {
        return mana;
    }

    public HeroClass getHeroClass() {
        return heroClass;
    }

    public int getDeckIndex() {
        return deckIndex;
    }

    public void setDeckIndex(int deckIndex) {
        this.deckIndex = deckIndex;
    }

    public static boolean isError() {
        return error;
    }

    public static void setError(boolean error) {
        CollectionState.error = error;
    }

    public static void setErrorMassage(String errorMassage) {
        CollectionState.errorMassage = errorMassage;
    }

    public void setPlayDeck(Deck playDeck) {
        this.playDeck = playDeck;
    }

    public Deck getPlayDeck() {
        return playDeck;
    }

    public boolean isShowUnLockCards() {
        return showUnLockCards;
    }

    public boolean isShowLockCards() {
        return showLockCards;
    }

    public Input getSearchInput() {
        return searchInput;
    }

    public void setSearchInput(Input searchInput) {
        this.searchInput = searchInput;
    }

    public ArrayList<UIRecImage> getContainWriteHeroName() {
        return containWriteHeroName;
    }
}
